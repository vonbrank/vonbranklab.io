---
title: Windows Terminal配置与美化
tags:
  - 教程
  - Windows
categories:
  - 教程
---



```json
        "list":
        [
            {
                // Make changes here to the powershell.exe profile.
                "guid": "{61c54bbd-c2c6-5271-96e7-009a87ff44bf}",
                "name": "Windows PowerShell",
                "commandline": "powershell.exe",
                "hidden": false
            },
            {
                // Make changes here to the cmd.exe profile.
                "guid": "{99BA5433-DF5F-A898-C8E0-78B8BA55F251}",
                "name": "Anaconda Prompt",
                "commandline": "cmd.exe /K C:\\ProgramData\\Anaconda3\\Scripts\\activate.bat",
				"icon": "C:\\ProgramData\\Anaconda3\\Menu\\anaconda-navigator.ico",
                "hidden": false
            },
            {
                // Make changes here to the cmd.exe profile.
                "guid": "{0A9CC558-3024-89F3-5313-2C72A1789FEA}",
                "name": "Anaconda Prompt Powershell",
                "commandline": "%windir%\\System32\\WindowsPowerShell\\v1.0\\powershell.exe -ExecutionPolicy ByPass -NoExit -Command C:\\ProgramData\\Anaconda3\\shell\\condabin\\conda-hook.ps1; conda activate;",
				"icon": "C:\\ProgramData\\Anaconda3\\Menu\\anaconda-navigator.ico",
                "hidden": false
            },
            {
                // Make changes here to the cmd.exe profile.
                "guid": "{51DE0527-D780-95DD-0699-47455E97DE1F}",
                "name": "git bash",
                "commandline": "C:\\Program Files\\Git\\bin\\bash.exe",
				"icon": "C:\\Program Files\\Git\\mingw64\\share\\git\\git-for-windows.ico",
                "hidden": false
            },
            {
                // Make changes here to the cmd.exe profile.
                "guid": "{0caa0dad-35be-5f56-a8ff-afceeeaa6101}",
                "name": "命令提示符",
                "commandline": "cmd.exe",
                "hidden": false
            },
            {
                "guid": "{b453ae62-4e3d-5e58-b989-0a998ec441b8}",
                "hidden": false,
                "name": "Azure Cloud Shell",
                "source": "Windows.Terminal.Azure"
            }
```

