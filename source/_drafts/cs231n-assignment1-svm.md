---
title: CS231n | 课程作业 Assignment1 | 支持向量机 SVM
tags:
    - 教程
    - 深度学习
    - Python
    - CS231n
categories:
    - 深度学习
    - 计算机视觉
katex: true
cover: 'https://z3.ax1x.com/2021/03/21/64GeH0.md.png'
---

线性分类器是比KNN算法强大不少的图像分类手段，该方法可以自然地延伸到神经网络和卷积神经网络上。其主要由评分函数 **（score function）** 和 **损失函数（loss function）** 组成。

Assignment1的第二部分是要求搭建一个基于SVM损失函数的线性分类器。

## 先决条件

### 线性代数先决条件

SVM损失函数的梯度计算涉及矩阵求导术。

矩阵乘法中的导数表示每个元素对函数值的贡献，其最常见的形式为：

 $$f=a^TXb$$ 

其中自变量 $X$ 为 $n\times m$ 的矩阵， $a$ 为长度为 $n$ 的行向量， $b$ 为长度为 $m$ 的列向量。

此只给出一个结论—— $f$ 对 $X$ 的导数为：

 $$\frac {\partial f} {\partial X} = ab^T$$ 

### Numpy先决条件



## SVM支持向量机



## 参考文献

+ https://zhuanlan.zhihu.com/p/21478575
+ https://zhuanlan.zhihu.com/p/24709748