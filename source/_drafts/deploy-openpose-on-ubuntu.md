---
title: 在Ubuntu下部署OpenPose
date: 2021-03-08 22:18:39
tags:
  - 教程
  - Linux
  - 深度学习
categories:
  - 教程
cover: https://s3.ax1x.com/2021/03/08/61Zu80.png

---

OpenPose人体姿态识别项目是美国卡耐基梅隆大学（CMU）基于卷积神经网络和监督学习并以caffe为框架开发的开源库。

最近在和朋友做计算机视觉方向的大一年度项目，需要使用OpenPose的Python API。由于OpenPose的Python API需要手动编译安装才能获得，而OpenPose本身已经是比较老的框架了，不论是在Windows下还是在Linux下的编译、安装与部署都需要处理大量兼容性问题与依赖项，由此写下本教程来记录我们在Ubuntu下针对OpenPose的安装过程、遇到的问题及其解决方法。

需要说明的是，本文不是一篇有针对性的教程，只是对遇到的问题进行记录，并对参考官方文档的安装方式和一些坑点进行说明。对于OpenPose来说，每台设备的不同环境可能导致编译过程中的不同问题，本文尽量讨论几类常见问题及其通用解决方案。



## 先决条件

### 安装CMake GUI

### NVIDIA GPU先决条件

#### **CUDA**

#### **cuDNN**

### 安装OpenCV和Caffe

#### OpenCV

#### Caffe

### Python先决条件

## 部署OpenPose

### 从Github上下载OpenPose源码

### CMake配置

### 编译

### 运行OpenPose