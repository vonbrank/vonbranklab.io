---
title: MySQL快速开始
tags:
    - 数据分析
    - 数据库
    - Web
categories:
    - 数据分析
    - 数据库
cover: 'https://z3.ax1x.com/2021/04/19/cTRwzd.md.jpg'
---

### 启动MySQL

切换目录：

```bash
cd {MySQL路径}\bin
```

初始化数据库：

```bash
mysqld --initialize --console
```

出现形如以下形式的信息：

```bash
...
2021-04-19T15:24:02.830281Z 6 [Note] [MY-010454] [Server] A temporary password is generated for root@localhost: Me3G#GfJwM2P
...
```

`Me3G#GfJwM2P`是初始密码，后续登录要用到。

安装：

```bash
mysqld install
```

启动：

```bash
net start mysql
```



### 登录MySQL

登陆本机数据库：

```bash
mysql -u root -p
```

得到以下响应，在后面输入密码即可：

```bash
Enter password:
```

出现形如以下的信息：

```bash
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 9
Server version: 8.0.23

Copyright (c) 2000, 2021, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>
```

之后每一行都以`mysq>`开头，可以输入`exit`或`quit`退出登录。