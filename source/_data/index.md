
## C/C++

### HIT-CS31106-课程答疑

[C语言答疑-01](..\_posts\hit-cs31106-explanation-01.md)

[C语言答疑-02](..\_posts\hit-cs31106-explanation-02.md)

[C语言答疑-03](..\_posts\hit-cs31106-explanation-03.md)

[C语言答疑-04](..\_posts\hit-cs31106-explanation-04.md)

## 教程

[Windows下的Visual Studio Code 配置指南（C/C++）](..\_posts\visual-studio-code-c-config-windows.md)

[如何将Ubuntu安装到U盘-UEFI引导](..\_posts\visual-studio-code-c-config-windows.md)

[Ubuntu 20.04 配置指南](..\_posts\ubuntu-20-04-config.md)

[Anaconda部署与使用指南（Windows）](..\_posts\deploy-anaconda-on-windows.md)

[利用Sympy求解常系数微分方程](..\_posts\sympy-solve-differential-equation.md)

[【搬运】Linux新手快速入门指南](..\_posts\linux-quick-start.md)

## 日常

[NOIP 2018 退役记]()

## 深度学习

### 计算机视觉

[CS231n | 先决条件 Preparation]()

[CS231n | 课程作业 Assignment1 | K近邻算法 KNN]()

## 编程竞赛 (Competitive Programming)

### 图论

[【OI考古】图论 | 最近公共祖先 LCA](../_posts/oi-graph-theory-strongly-connected-components.md)
[]