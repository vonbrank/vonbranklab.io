---
title: Bullet Journal | 子弹笔记 | 2021
tags:
  - 私人领域
categories:
  - 日常
description: VonBrank的2021年度子弹笔记
katex: true
cover: 'https://z3.ax1x.com/2021/05/02/gZ1Ppt.md.png'
date: 2021-05-01 00:00:00
---

[latest](#latest)

## Future Log

### APR

### 𝖬AY

[Monthly Log](#MAY)

+ Java开发
+ Javascript开发
+ Excel学习
+ Python应用

### JUN

[Monthly Log](#JUNE)

+ Web开发实战
  + 静态页面
  + 模仿

+ PhotoShop实例

### JUL

[Monthly Log](#JULY)

+ CHOSER STUDIO网站重构

### AUG

+ 和Louis旅游：云南
+ Blender入门

### SEPT

+ 计算机图形学入门：GAMES 101

### OCT

### NOV

### DEC

+ 买iPad

## Monthly Log

### MAY
[Future Log](#𝖬AY)

|       一        |        二        |       三        |       四        |                   五                   |                  六                   |           日           |
|:---------------:|:----------------:|:---------------:|:---------------:|:--------------------------------------:|:-------------------------------------:|:----------------------:|
|                 |                  |                 |                 |                                        |            [1](#05-01-SAT)            |    [2](#05-02-SUN)     |
| [3](#05-03-MON) | [4](#05-04-TUE)  | [5](#05-05-WED) | [6](#05-06-THU) | [7](#05-07-FRI)<br>期中考试 <br>近代史 | [8](#05-08-SAT)<br>期中考试<br>微积分 |    [9](#05-09-SUN)     |
|       10        |        11        |       12        |       13        |                   14                   |                  15                   |           16           |
|       17        |        18        |       19        |       20        |                   21                   |                  22                   | 23<br>英语六级<br>口语 |
|       24        | [25](#05-25-TUE) |       26        |       27        |                   28                   |                  29                   |           30           |
|       31        |                  |                 |                 |                                        |                                       |                        |

+ CS231n Assignments
  - 深度学习框架——PyTorch
  - CNN
  - 生成模型

+ 口述作文训练
+ 每日六级单词 $\times 40$ 
+ 日常CET-6训练
+ HTML/CSS复习
+ Javascript开发
+ Java开发
+ Android开发入门
+ Python爬虫
+ 数据分析入门
  - Excel
  - SPSS
  - Python

+ GitGraph项目
+ 练字
  - 从楷书向行楷过渡
  - 意大利斜体
  - 花体

+ 博客更新
  - 自动部署
  - OI考古系列
  - 摄影创作笔记系列
  - 自动化快速笔记系统
  - CS231n作业

+ 课程进度与作业同步

#### 05-01 SAT

[Monthly Log](#MAY)

+ `[x]` 微积分课本题：8.4/8.5/8.6
+ `[ ]` 集合论课本题
+ `[>]` 大物：振动作业
+ `[>]` 历史论文范例
+ `[>]` CET-6单词 $\times 40$
+ `[x]` CET6-2018-12月-作文：口述与标答朗读
+ `[>]` 练字：楷 $\to$ 行楷
+ `[>]` 微积分：第一型曲线积分
+ `[>]` 博客：SVM，高精度算法，Linux
+ `[x]` 六级口语相关资料
+ `[x]` HTML复习

#### 05-02 SUN

[Monthly Log](#MAY)

+ `[>]` 微积分课本题：8.6/8.7/8.8
+ `[>]` 微积分期中模拟 $\times 1$ 
+ `[>]` 论文范例
+ `[x]` CET6单词 $\times 40$ 
+ `[x]` CET6-2018-12月（二）-作文：口述与标答朗读
+ `[x]` 博客：高精度算法，BUJO
+ ~~`[ ]` 大物：振动作业~~
+ ~~`[ ]` 微积分：第一型曲线积分~~
+ `[x]` 练字：楷 $\to$ 行楷
+ `[x]` HTML/CSS复习
+ `[>]` 旅行计划

#### 05-03 MON

[Monthly Log](#MAY)

+ `[x]` 微积分课本题： $8.6/8.7/8.8$
+ `[>]` 微积分课本题： $9.1/9.2$
+ `[x]` 微积分期中模拟 $2012$ 
+ `[x]` CET6单词 $\times 40$ 
+ `[x]` CET6-2018-12月（三） - 作文：口述与标答朗读
+ `[x]` HTML/CSS复习
+ `[x]` 练字：楷 $\to$ 行楷
+ `[>]` 旅行计划
+ `[x]` 换床单被套
+ `[x]` 论文范例

#### 05-04 TUE

[Monthly Log](#MAY)

+ `[x]` 微积分课本题： $9.1/9.2$
+ `[x]` 旅行计划
+ `[x]` CET6单词 $\times 40$ 
+ `[>]` CET6-2019-6月（一） - 作文：口述与标答朗读
+ `[x]` 微积分期中模拟 $2013$ 
+ `[x]` 近代史复习
+ `[x]` 微积分-微分方程复习
+ `[>]` 练字：意大利斜体/花体
+ `[x]` HTML/CSS复习

#### 05-05 WED

[Monthly Log](#MAY)

+ `[x]` 微积分课本题： $9.3/7.4/7.5$
+ `[x]` 微积分 | 期中真题 $2014$ $/$ 期中模拟 $Ⅰ$ 
+ `[>]` 微积分疑难解答
+ `[x]` 练字：意大利斜体/花体
+ `[x]` HTML/CSS复习
+ `[x]` 大一年度项目中期PPT
+ ~~`[ ]` CET6单词 $\times 40$~~
+ ~~`[ ]` CET6-2019-6月（一） - 作文：口述与标答朗读~~
+ `[>]` 集合论：
  + 基数的比较
  + 康托伯恩斯坦定理
+ `[>]` 图论：
  + 图的基本概念
+ ~~`[ ]` 博客：高精度减法~~

#### 05-06 THU

[Monthly Log](#MAY)

+ `[x]` 微积分期中模拟 $Ⅱ \ Ⅲ$ 
+ `[>]` 微积分疑难解答
+ `[>]` 微积分课本题： $9.3$ 
+ `[x]` HTML/CSS复习A
+ `[x]` 近代史大作业材料阅读

#### 05-07 FRI

[Monthly Log](#MAY)

+ `[x]` 微积分期中模拟 $Ⅳ \ Ⅴ \ Ⅵ \ Ⅶ$
+ ~~`[ ]` 微积分课本题： $9.3$~~
+ `[>]` 大物振动预览
+ `[x]` 微积分疑难解答

#### 05-08 SAT

[Monthly Log](#MAY)

+ `[⚬]` 微积分-期中考试
+ ~~`[ ]` 打网球~~
+ `[x]` 大物振动预览
+ `[x]` 大物振动作业
+ `[>]` 集合论：
  + 基数的比较
  + 康托伯恩斯坦定理
+ `[>]` 图论：
  + 图的基本概念
+ `[x]` 玩全境封锁2
+ `[x]` 玩Portal2

#### 05-09 SUN

[Monthly Log](#MAY)

+ `[x]` 集合论：
  + 基数的比较
  + 康托伯恩斯坦定理
+ `[x]` 图论：
  + 图的基本概念
+ `[>]` 图论 - $6.2/6.3/6.4/6.5$ 课本题
+ ~~`[ ]` 微积分 - $9.4$ 课本题~~
+ ~~`[ ]` 打网球~~
+ ~~`[ ]` CET6单词 $\times 40$~~
+ `[x]` CET6-2019-6月（一） - 作文：口述与标答朗读
+ `[>]` CET6口语训练 - B站 
+ ~~`[ ]` 练字：楷书 $\to$ 行书~~
+ `[x]` 讯飞AI口语

#### 05-10 MON

[Monthly Log](#MAY)


+ `[>]` 图论 - $6.2/6.3/6.4/6.5$ 课本题
+ `[x]` 图论：连通，欧拉图，哈密顿图
+ `[x]` 微积分：$9.4/9.5$ ；课本题
+ `[>]` CET6-2019-6月（二） - 作文：口述与标答朗读
+ `[>]` CET6口语训练 - B站 
+ ~~`[ ]` CET6单词 $\times 40$~~
+ `[x]` 大物：振动预览
+ `[>]` 大物振动作业
+ ~~`[ ]` 翻译作业修正~~

#### 05-11 TUE

[Monthly Log](#MAY)


+ `[>]` CET6单词 $\times 40$
+ `[>]` CET6口语训练 - B站 
+ `[>]` CET6-2019-6月（二） - 作文：口述与标答朗读
+ `[>]` 微积分： $9.5/9.6/10.1$ 课本题
+ `[x]` 大物振动作业
+ `[x]` NVIDIA网站RWD重构
+ `[>]` 图论：连通，欧拉图，哈密顿图
+ ~~`[ ]` 翻译作业~~

#### 05-12 WED

[Monthly Log](#MAY)


+ `[x]` CET6单词 $\times 40$
+ `[x]` CET6口语训练 - B站
+ ~~`[ ]` CET6-2019-6月（二） - 作文：口述与标答朗读~~
+ `[>]` 讯飞AI口语
+ `[x]` 高级口语演讲稿
+ `[x]` 微积分： $9.5/9.6/10.1$ 课本题
+ `[>]` 微积分： $9.5/9.6$ 作业
+ `[x]` 图论：连通，欧拉图，哈密顿图
+ `[x]` NVIDIA网站重构
+ ~~`[ ]` SPSS学习~~

#### 05-13 THU

[Monthly Log](#MAY)


+ `[>]` CET6单词 $\times 40$
+ `[x]` CET6口语训练 - B站
+ `[x]` 高级口语PPT
+ `[>]` 讯飞AI口语
+ `[x]` 微积分： $9.5/9.6$ 作业
+ `[>]` 微积分： $9.5/9.6/$ 课本题
+ `[>]` 微积分： $10.1/10.2$ 课本题
+ `[x]` JavaScript快速开始
+ `[>]` 大物-机械波
+ `[x]` 去一校区找导师签字
+ `[>]` 图论：欧拉图，哈密顿图 - 课本题

#### 05-14 FRI

[Monthly Log](#MAY)

+ ~~`[ ]` CET6单词 $\times 40$~~
+ `[>]` 讯飞AI口语
+ `[x]` 微积分： $9.5/9.6$ 作业
+ ~~`[ ]` 微积分： $9.5/9.6/$ 课本题~~
+ `[>]` 大物-机械波
+ `[>]` 图论：欧拉图，哈密顿图 - 慕课
+ `[>]` 图论：欧拉图，哈密顿图 - 课本题
+ `[x]` JavaScript继续

#### 05-15 SAT

[Monthly Log](#MAY)

+ `[⚬]` 高级口语结课考试 
+ ~~`[ ]` 讯飞AI口语~~
+ `[>]` 大物-机械波 - 预览
+ `[x]` 图论：欧拉图，哈密顿图 - 慕课
+ `[>]` 图论：欧拉图，哈密顿图 - 课本题
+ `[x]` 玩MC
+ ~~`[ ]` JavaScript继续~~
+ `[x]` 专业解读 - 期末作业

#### 05-16 SUN

[Monthly Log](#MAY)

+ `[⚬]` 年度项目中期答辩
+ `[>]` 大物-机械波 - 预览
+ `[x]` 图论：欧拉图，哈密顿图 - 课本题
+ `[>]` 微积分： $10.1/10.2$ 慕课
+ `[>]` 微积分： $10.1/10.2$ 课本题
+ `[>]` CET6口语课


#### 05-17 MON

[Monthly Log](#MAY)

+ `[x]` 微积分： $10.1/10.2$ 慕课
+ `[>]` 微积分： $10.1/10.2$ 课本题
+ `[x]` CET6口语课
+ `[>]` CET6单词 $\times 40$
+ `[>]` CET6长阅读 $\times 1$
+ `[x]` 大物-机械波 - 预览
+ `[>]` 大物-机械波 - 作业
+ `[x]` 图论：欧拉图，哈密顿图 - 课本题
+ `[x]` 图论：树 - 课本题
+ `[>]` 图论：连通度 - 慕课
+ `[x]` Android开发 - 第一行代码
+ `[x]` JavaScript学习
+ `[>]` 年度项目开会

#### 05-18 TUE

[Monthly Log](#MAY)

+ `[x]` 微积分： $10.1/10.2$ 课本题
+ ~~`[ ]` CET6单词 $\times 40$~~
+ `[>]` CET6长阅读 $\times 1$
+ `[x]` 大物-机械波 - 作业
+ `[x]` 图论：连通度 - 慕课
+ `[x]` CET6口语课
+ `[x]` 年度项目开会

#### 05-19 WED

[Monthly Log](#MAY)

+ `[x]` CET6单词 $\times 40$
+ `[>]` CET6长阅读 $\times 1$
+ `[x]` 大物-机械波 - 作业 - 写完
+ `[x]` 大物-静电场 - 预览
+ `[>]` CET6口语课
+ `[x]` 图论：连通度 - 慕课
+ `[x]` 图论：匹配 - 慕课
+ `[>]` 图论-树 - 课本题
+ `[x]` 微积分： $10.3/10.4$ - 慕课
+ `[>]` Unity快速开始
+ `[>]` DOM树操作


#### 05-20 THU

[Monthly Log](#MAY)

+ `[>]` CET6单词 $\times 40$
+ `[>]` 图论-树 - 课本题
+ `[>]` 微积分： $10.3/10.4$ - 课本题
+ `[>]` CET6口语课
+ `[>]` DOM树操作
+ `[x]` Gitbook
+ `[>]` Blog - 高精度减/乘/除法
+ `[>]` CET6长阅读 $\times 1$

#### 05-21 FRI

[Monthly Log](#MAY)

+ ~~`[ ]` CET6单词 $\times 40$~~
+ `[x]` 图论-树 - 课本题
+ `[>]` 微积分： $10.3/10.4$ - 课本题
+ `[x]` CET6口语课
+ `[x]` DOM树操作
+ `[>]` Blog - 高精度减/乘/除法
+ `[ ]` Blog - 割点、桥
+ `[>]` CET6长阅读 $\times 1$
+ `[>]` 大物-静电场 - 作业
+ `[x]` CET6口语准考证打印
+ `[x]` 摄影创作实践课准备
+ `[x]` 玩双人成行

#### 05-24 MON

[Monthly Log](#MAY)

+ `[x]` CET6单词 $\times 40$
+ `[>]` CET6长阅读 $\times 1$
+ `[>]` CET6听力 $\times 1$
+ `[x]` 大物-静电场 - 作业
+ `[x]` 大物-大作业 - 提纲
+ `[x]` 微积分： $10.3/10.4$ - 课本题
+ `[>]` 微积分： $10.5/10.5$ - 慕课
+ `[x]` 摄影创作实践课答案上传
+ `[x]` **Unity入门**
+ `[x]` 图论-连通度和匹配 - 作业
+ `[>]` Blog - 高精度减法

#### 05-25 TUE

[Monthly Log](#MAY)

+ `[>]` 微积分： $10.5/10.6$ - 慕课
+ `[>]` 图论-匹配 - 作业
+ `[>]` 图论-平面图 - 慕课
+ `[>]` CET6单词 $\times 40$
+ `[x]` 英语翻译简明教程 - P15-38
+ `[>]` **Unity入门**
+ `[>]` 图论 - 散装作业
+ `[>]` 大物-静电场 - 作业
+ `[>]` CET6长阅读 $\times 1$
+ `[>]` CET6听力 $\times 1$
+ `[>]` Excel入门
+ `[x]` Blog - 高精度减法

#### 05-26 WED

[Monthly Log](#MAY)

+ `[x]` 微积分： $10.5/10.6$ - 慕课
+ `[>]` 图论-匹配 - 作业
+ `[x]` 图论-平面图 - 慕课
+ `[x]` CET6单词 $\times 40$
+ `[x]` **Unity入门**
+ `[x]` 图论 - 散装作业
+ `[>]` 大物-静电场 - 作业
+ `[>]` CET6长阅读 $\times 1$
+ `[x]` CET6听力 $\times 1$
+ `[>]` Excel入门

#### 05-27 THU

[Monthly Log](#MAY)

+ `[>]` CET6单词 $\times 40$
+ `[>]` CET6长阅读 $\times 1$
+ `[>]` 英语翻译简明教程 - P15-38
+ `[x]` 图论-匹配 - 作业
+ `[>]` 图论-平面图 - 慕课
+ `[>]` **Unity入门**
+ `[>]` 图论 - 散装作业
+ `[>]` 大物-静电场Ⅱ - 作业
+ `[>]` Excel入门
+ `[x]` 微积分： $10.4/10.4$ - 作业、课本题

#### 05-28 FRI

[Monthly Log](#MAY)

+ ~~`[ ]` CET6单词 $\times 40$~~
+ ~~`[ ]` CET6长阅读 $\times 1$~~ 
+ ~~`[ ]` 英语翻译简明教程 - P15-38~~
+ ~~`[ ]` 图论-平面图 - 慕课~~
+ ~~`[ ]` 图论-染色 - 慕课~~
+ ~~`[ ]` **Unity入门**~~
+ ~~`[ ]` 图论 - 散装作业~~
+ `[x]` 大物-静电场Ⅱ - 作业
+ ~~`[ ]` Excel入门~~


#### 05-30 SAT

[Monthly Log](#MAY)

+ `[ ]` CET6单词 $\times 40$
+ `[ ]` CET6长阅读 $\times 1$
+ `[ ]` **Unity入门**
+ `[x]` 图论慕课作业
+ `[ ]` 图论 - 散装作业
+ `[x]` 图论 - 平面图、染色、有向图
+ `[ ]` 微积分 - $10.5/10.6/10.7$ 课本题
+ `[x]` 微积分 - $10.5$ 作业题

### JUNE

| 一 | 二 |       三        | 四 | 五 |       六        |           日           |
|:--:|:--:|:---------------:|:--:|:--:|:---------------:|:----------------------:|
|    | 1  | [2](#06-02-WED) | 3  | 4  | 5<br>蓝桥杯国赛 |           6            |
| 7  | 8  |        9        | 10 | 11 | 12<br>CET6-笔试 |           13           |
| 14 | 15 |       16        | 17 | 18 |       19        | 20<br>集合论与图论考试 |
| 21 | 22 |       23        | 24 | 25 |       26        |           27           |
| 28 | 29 |       30        |    |    |                 |                        |

+ Android开发入门
+ Python爬虫
+ 数据分析入门
  - Excel
  - SPSS
  - Python
+ Git Graph项目
+ Javascript - DOM 编程艺术
+ Web开发实战
+ 静态页面模仿
+ PhotoShop实例
+ 物理大作业

#### Week 23

+ 微积分：第二型积分 - 课本 + 作业练习
+ 微积分：级数 - 课本 + 作业练习
+ 大物：静电场 - 作业练习
+ 大物：稳恒磁场 - 作业练习
+ 大物：大作业
+ 集合论和图论：课本 + 作业练习
+ JavaScript - DOM 编程艺术：4 ~ 6章
+ Photoshop实例：1 ~ 4
+ Excel：入门
+ 博客：
  - 二分图匹配
  - 拓扑排序	
  - After Effects快速开始
+ Unity：入门

#### 06-02 WED

+ `[x]` 微积分：级数 - 慕课
+ `[x]` 微积分：第二型积分课本题
+ `[x]` 微积分：第二型积分 $10.6/10.7$ 课本题
+ `[>]` 大物：静电场作业练习
+ `[x]` 图论：比赛图 - 慕课
+ `[x]` 图论：散装作业
+ `[>]` 大物：大作业 - 任务分配
+ `[x]` CET6：笔试 $\times 1$ 

#### 06-04 FRI
+ `[ ]` 大物：大作业 - 任务分配
+ `[x]` 大物：静电场 - 作业练习
+ `[ ]` 大物：磁场 - 作业练习
+ `[>]` 微积分： $10.8$ 课本题
+ `[>]` 微积分：正向级数敛散性判别法、任意项幂级数、绝对收敛 - 慕课
+ `[>]` 图论：慕课练习
+ `[x]` 图论：散装作业
+ `[x]` 蓝桥杯：准考证打印
+ `[>]` CET6：阅读 + 翻译
+ `[x]` JavaScript - DOM 编程艺术：4.4 ~ 5.3

#### 06-05 SAT
+ `[x]` 微积分：正向级数敛散性判别法、任意项幂级数、绝对收敛 - 慕课
+ `[x]` CET6：阅读 + 翻译
+ `[x]` 图论：慕课练习
+ `[x]` 图论：课本练习
+ ~~`[ ]` 微积分： $10.8$ 课本题~~
+ `[x]` 大物：大作业 - 看资料
+ `[x]` JavaScript - DOM 编程艺术：6.1 ~ 6.9


#### Week 24

+ 大物：大作业
+ 大物：稳恒磁场 - 作业练习
+ 图论：集合论 - 课本练习 + PPT练习
+ 集合论和图论：2009-2012真题
+ Photoshop实例：1 ~ 4
+ 六级真题 x2
+ 微积分：级数 - 课本练习
+ 翻译实践：50%
+ Bootstrap练习

#### 06-08 TUE

+ `[>]` 微积分：$11.2/11.3$ 课本题
+ `[x]` 图论：慕课期末
+ `[>]` 大物：磁场 - 作业练习Ⅰ
+ `[x]` 六级听力 x1`
+ `[ ]` 六级选词填空 x1
+ `[>]` 图论：第十章 - 课本题
+ `[x]` 大物：大作业 - c-v

#### 06-09 WED

+ `[>]` 图论：第十章 - 课本题
+ `[>]` 微积分：$11.2/11.3$ 课本题
+ `[x]` 微积分：幂级数、函数幂级数展开与求和 - 慕课
+ `[x]` 六级：真题 x1
+ `[ ]` 大物：磁场 - 作业练习Ⅰ
+ ~~`[ ]` Bootstrap~~
+ `[x]` 校车刷跑
+ `[>]` 英语翻译资料打印

#### 06-10 THU

+ `[x]` 图论：第十章 - 课本题
+ `[x]` 微积分：$11.2/11.3$ 课本题
+ `[ ]` 微积分：函数幂级数展开与求和 - 慕课
+ `[x]` 大物：磁场 - 作业练习Ⅰ
+ `[ ]` 大物：大作业 - 完结
+ `[x]` 英语翻译资料打印
+ `[ ]` 集合论和图论： 2013年真题
+ ~~`[ ]` 免跑活动~~
+ `[x]` 买收音机

#### 06-11 FRI

+ `[x]` 微积分：函数幂级数展开与求和 - 慕课
+ `[x]` 大物：大作业 - 完结
+ `[x]` 集合论和图论： 2013年真题
+ `[x]` 六级英语2020-12 - 听力精听
+ `[x]` 六级英语2020-12 - 剩下写完
+ `[x]` 集合论复习 - 第一章、第二章
+ `[x]` 集合论-第二章：课本题写完
+ `[x]` 微积分：$11.3$ 课本题
+ `[x]` Bootstrap 娱乐
+ `[x]` 六级准考证打印

#### Week 26

+ 大物：课本题 + 练习题
+ 微积分：课本题 + 作业题 + 微分方程课本题
+ 近代史：史实
+ Java教程：Michael Liao | 面向对象

#### 06-23 WED

+ `[x]` 微积分： $11.3/11.4/11.5$ 课本题 + 作业题
+ `[>]` 大物： 电磁感应、电磁波 作业练习
+ `[>]` Java：构造方法、方法重载、继承、多态、抽象类、接口
+ `[>]` Bootstrap：按钮组、徽章、进度条、分页

#### 06-24 TUE

+ `[x]` 微积分： $11.6/11.7/11.8$ 课本题 + 作业题
+ `[ ]` 大物： 电磁感应、电磁波 作业练习
+ `[ ]` Java：构造方法、方法重载、继承、多态、抽象类、接口
+ `[ ]` Bootstrap：按钮组、徽章、进度条、分页
+ `[ ]` 微积分期末模拟（一）
+ `[ ]` 大物2010期末
+ 


### JULY

[Future Log](#JUL)

| 一 | 二 | 三 | 四 | 五 | 六 | 日 |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
|    |    |    | 1  | 2  | 3  | 4  |
| 5  | 6  | 7  | 8  | 9  | 10 | 11 |
| 12 | 13 | 14 | 15 | 16 | 17 | 18 |
| 19 | 20 | 21 | 22 | 23 | 24 | 25 |
| 26 | 27 | 28 | 29 | 30 | 31 |    |

+ Android开发入门
+ Python爬虫
+ 数据分析入门
  - Excel
  - Python
+ Git Graph项目
+ Javascript - DOM 编程艺术
+ Web开发实战
+ CHOSER Tech Group网站重构
+ 静态页面模仿
+ PhotoShop实例
+ 六级单词、六级听力练习


#### Week 29

+ JavaScript DOM编程艺术： $7.1 \sim 8.8$
+ 离散数学引论答案补充：第一章
+ PhotoShop实例： $1 \sim 4$
+ Java：面向对象、异常处理、反射、注解、泛型、集合、IO
+ Bootstrap
+ 博客：二分图匹配、最小生成树、拓扑排序、欧拉回路

#### 07-12 MON

+ `[x]` JavaScript DOM编程艺术： $7.1 \sim 7.3$
+ `[x]` Bootstrap：按钮组、徽章、进度条、分页
+ `[x]` 博客：二分图匹配
+ `[x]` Java：面向对象编程
+ `[x]` CHOSER STUDIO需求检查

#### 07-16 FRI

+ `[x]` Java：异常处理、反射
+ `[>]` 博客：最小生成树
+ `[x]` JavaScript DOM编程艺术： $7.4 \sim 8.3$
+ `[x]` Bootstrap：列表组、卡片、下拉菜单、折叠
+ `[x]` PhotoShop：入门教程
+ `[x]` 了解黄书系列

#### 07-18 SUN

+ `[ ]` 博客：最小生成树、拓扑排序
+ `[ ]` 离散数学引论答案补充：第一章
+ `[ ]` Java：泛型、集合
+ `[x]` Bootstrap：导航、导航栏、面包屑导航、表单
+ `[x]` PhotoShop教程：doyoudo一分钟系列S1
+ `[x]` 概率论基础教程： $1.1 \sim 1.3$ 


#### Week 30 

+ SN550-1TB 部署
+ BuildNNEZ空岛活动：建筑现代化
+ JavaScript DOM编程艺术： $9.1 \sim 10.3$
+ 思政实践课作业
+ Web前后端分离：Ajax + Servlet
+ Excel入门
+ 概率论基础教程：第 $2$ 章 
+ Java：反射、注解、


#### 07-22 THU

+ `[x]` Servlet登录验证
+ `[>]` 练字
+ `[ ]` BuildNNEZ空岛活动设计：实验室、通道、电梯、摆件
+ `[x]` Bootstrap：表单控件、输入框组、自定义表单、轮播
+ `[x]` 思政实践课视频拍摄

#### 07-23 FRI

+ `[x]` 练字
+ `[x]` Java：反射
+ `[ ]` Excel入门
+ `[x]` Web前后端分离：Ajax + Servlet
+ `[x]` 概率论基础教程：第 $2$ 章 



### AUGUST

[Future Log](#AUG)

| 一 | 二 | 三 | 四 | 五 | 六 | 日 |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
|    |    |    |    |    |    | 1  |
| 2  | 3  | 4  | 5  | 6  | 7  | 8  |
| 9  | 10 | 11 | 12 | 13 | 14 | 15 |
| 16 | 17 | 18 | 19 | 20 | 21 | 22 |
| 23 | 24 | 25 | 26 | 27 | 28 | 29 |
| 30 | 31 |    |    |    |    |    |

+ Android开发入门
+ Python爬虫
+ `[>]` 数据分析入门
    - Excel
    - Python
+ Git Graph项目
+ Javascript - DOM 编程艺术
+ Web开发实战
+ CHOSER Tech Group网站重构
+ 静态页面模仿
+ PhotoShop实例
+ 六级单词、六级听力练习
+ Blender入门
+ 集合论答案收集
+ 高级数据结构
+ 前后端框架

#### Week 32


+ PhotoShop实例： 小白教程第二季
+ 六级单词、六级听力练习
+ Java：泛型、集合、IO、
+ Python爬虫入门
+ Android开发入门
+ 概率论基础教程：第 $3 , \ 4$ 章 
+ JavaScript DOM编程艺术： $9.1 \sim 12.6$

#### 08-14 SAT

+ `[>]` 概率论基础教程：第 $2$ 章 练习
+ `[x]` JavaScript DOM编程艺术第12章实现
+ `[x]` Python爬虫实现
+ `[x]` 六级单词 $\times 30$
+ `[x]` 四级听力 $\times 1$
+ `[x]` Android开发入门

#### 08-18 WED

+ `[x]` 概率论基础教程：第 $2$ 章 练习
+ `[>]` PhotoShop实例： 小白教程第二季
+ `[>]` Java：泛型、集合、IO
+ `[x]` 六级单词 $\times 30$
+ `[x]` 四级听力 $\times 1$
+ `[x]` SkecthUp 家具建模
+ `[x]` 第一行代码：第2章
+ `[x]` JavaEE笔记Ⅰ

#### 08-21 SAT

+ `[x]` PhotoShop实例： 小白教程第二季
+ `[x]` Java：泛型、集合、IO
+ `[x]` 概率论基础教程：第 $3$ 章 知识点
+ `[x]` SkecthUp 组合建模
+ `[x]` 六级单词 $\times 30$
+ `[x]` 六级真题 $\times 0.5$
+ `[x]` 第一行代码：第3章


#### 08-25 WED

+ `[x]` 六级单词 $\times 30$
+ `[x]` 六级真题 $\times 0.5$
+ `[x]` 第一行代码：第4章
+ `[x]` Java：IO、日期、单元测试、加密、多线程
+ `[x]` 概率论基础教程： $3.3 \sim 3.5$  知识点
+ `[>]` C#入门
+ `[x]` Unity书籍准备
+ `[>]` 游戏开发入门
+ `[x]` Treap博客
+ `[x]` Splay学习


### SEPTEMBER

[Future Log](#SEPT)

| 一 | 二 | 三 | 四 | 五 | 六 | 日 |
|:--:|:--:|:--:|:--:|:--:|:--:|:--:|
|    |    | 1  | 2  | 3  | 4  | 5  |
| 6  | 7  | 8  | 9  | 10 | 11 | 12 |
| 13 | 14 | 15 | 16 | 17 | 18 | 19 |
| 20 | 21 | 22 | 23 | 24 | 25 | 26 |
| 27 | 28 | 29 | 30 |    |    |    |

+ 六级单词 $\to 2000$ 个
+ 四级听力掌握
+ 每周末两套六级题
+ C# 入门经典 $1 \sim 13$ 章
+ Unity 2D独立游戏开发 $1 \sim 3$ 章
+ SketchUp 渲染
+ Blender入门
+ JavaScript高级程序设计 $1 \sim 6$ 章
+ 数据分析入门
  - Excel
  - Python
+ 集合论答案收集 $1 \sim 3$ 章
+ CSP真题写完
+ PhotoShop实例
+ Build NNEZ 项目完成
+ Java EE笔记
+ 概率论基础教程 $4 \sim 6$ 章

#### Week 37

+ C# 入门经典 $1 \sim 6$ 章
+ Unity 2D独立游戏开发 $1 \sim 2$ 章
+ SketchUp 渲染
+ JavaScript高级程序设计 $1 \sim 3$ 章
+ CSP真题： $201903 \sim 202104$
+ 每日六级词 $\times 30$ 
+ 每日四级精听 $\times 1$
+ 六级真题 $\times 2$
+ Build NNEZ 项目，宿舍中庭与外围校道
+ 概率论基础教程第 $4$ 章

#### 09-06 MON

+ `[x]` 六级单词 $\times 30$ 
+ `[>]` 四级听力 Section $\times 1$
+ `[x]` SkecthUp 室内建模
+ `[>]` Splay 实现
+ `[>]` 红黑树原理
+ `[>]` Build NNEZ 项目外围校道围栏路径
+ `[x]` C# 入门经典 $1 \sim 2$ 章
+ `[x]` 大创项目讨论
+ `[>]` 概率论基础教程：第三章 $31 \sim 60$ 题； $4.1$ 章
+ `[x]` 练字

#### 09-07 TUE

+ `[x]` 六级单词 $\times 30$ 
+ `[x]` 四级听力 Section $\times 1$
+ `[x]` Splay 实现
+ `[x]` 数据结构作业
+ `[x]` 概率论基础教程：第三章 $31 \sim 60$ 题； $4.1 \sim 4.2$ 章
+ `[>]` CSP-202104-真题
+ `[>]` SkecthUp 第七章室内建模完成
+ `[x]` 练字

#### 09-09 THU

+ `[x]` 六级单词 $\times 30$ 
+ `[x]` 四级听力 Section $\times 1$
+ `[ ]` CSP-202104-真题
+ `[x]` SkecthUp 第七章起泡泡插件
+ `[x]` JavaScript高级程序设计 $1 \sim 2$ 章
+ `[x]` C# 入门经典 $3 \sim 5$ 章
+ `[>]` Unity 2D独立游戏开发第 $1$ 章
+ `[x]` 练字 
+ `[x]` 概率论基础教程：第三章 $3.31 \sim 3.60$ 题
+ `[>]` 概率论与数理统计：习题 $1.1 \sim 1.15$ 


#### 09-11 SAT

+ `[>]` 六级单词 $\times 30$ 
+ `[>]` 四级听力 Section $\times 1$
+ `[x]` 六级真题 $\times 0.5$
+ `[>]` 数据结构作业提交
+ `[x]` C# 入门经典 $6 \sim 10$ 章
+ `[>]` CSP-202104、202012、202009-水真题
+ `[>]` Unity 2D独立游戏开发第 $1 \sim 2$ 章
+ `[x]` 打乒乓球



#### 09-12 SUN

+ `[x]` 六级单词 $\times 30$ 
+ `[>]` 四级听力 Section $\times 1$
+ `[x]` 数据结构作业提交
+ `[x]` CSP-202104、202012、202009-水真题
+ `[x]` Unity 2D独立游戏开发第 $1 \sim 2$ 章
+ `[>]` Build NNEZ 项目，宿舍中庭与外围校道
+ `[x]` C# 入门经典 $11 \sim 13$ 章
+ `[>]` 概率论基础教程： $4.5 \sim 4.9$
+ `[>]` 毛概序章背书

#### Week 38

+ C# 入门经典 $7 \sim 10$ 章
+ Unity 2D独立游戏开发 $2 \sim 3$ 章
+ JavaScript高级程序设计 $4 \sim 7$ 章
+ CSP真题： $201612 \sim 201812$
+ 每日六级词 $\times 30$ 
+ 每日四级精听 $\times 1$
+ 六级真题 $\times 2$
+ Build NNEZ 项目，宿舍区域植树与校道植树
+ 设计模式

#### 09-13 MON

+ `[>]` 概率论基础教程： $4.5 \sim 4.9$
+ `[ ]` 毛概序章背书
+ `[x]` 六级单词 $\times 30$ 
+ `[x]` 四级听力 Section $\times 1$
+ `[x]` 数据结构：第二章-作业1
+ `[x]` CSP真题： $201612 \sim 201712$
+ `[>]` Splay实现
+ `[>]` JavaScript高级程序设计 $4 \sim 5$ 章
+ `[x]` 设计模式Ⅰ


#### 09-17 FRI

+ `[ ]` 概率论基础教程： $4.5 \sim 4.9$
+ `[>]` JavaScript高级程序设计 $4 \sim 5$ 章
+ `[>]` 设计模式Ⅱ
+ `[>]` 六级单词 $\times 30$ 
+ `[>]` 四级听力 Section $\times 1$
+ `[ ]` CSP真题
+ `[ ]` 概率论第一章作业
+ `[ ]` Splay实现


+ `[>]` 六级单词 $\times 30$ 
+ `[>]` 四级听力 Section $\times 1$
+ `[x]` 联系大创老师
+ `[x]` 概率论第二章作业
+ `[x]` 组装自行车
+ `[x]` 一刻钟学会游戏开发基础；第一、二章
+ `[x]` JavaScript高级程序设计 $4 \sim 5$ 章
+ ~~`[ ]` 设计模式Ⅱ~~
+ `[x]` 英语语音作业一  


+ `[ ]` 六级单词 $\times 30$  
+ `[ ]` 四级听力 Section $\times 1$  
+ `[x]` 记账
+ `[x]` 一刻钟学会游戏开发基础；第二、三章
+ `[x]` C++ Primer Plus：第 $2 \sim 4$ 章
+ `[x]` 买东西：箱子、文具收纳、Uno
+ `[ ]` 十月规划
+ `[ ]` 数字逻辑：Verilog 语言

### OCTOBER

+ 一刻钟学会游戏开发基础
+ 每日六级词 $\times 30$
+ 每日四级精听 $\times 1$
+ 六级口语练习
+ Build NNEZ 项目完成
+ 设计模式
+ 廖雪峰JavaScript
+ JavaScript高级程序设计
+ C++ Primer Plus
+ 逻辑设计与计算机
+ 概率论基础教程： $4 \sim 6$ 章  
+ 鸟哥的Linux私房菜



#### 10-02 SAT

+ `[x]` C++ Primer Plus： 第 $3、4$ 章
+ `[>]` 一刻钟学会游戏开发基础： 第 $2、3$ 节
+ `[>]` 六级词 $\times 30$
+ `[x]` 四级精听 $\times 1$
+ `[ ]` Build NNEZ 项目：外校道
+ `[x]` 概率论基础教程第 $4$ 章
+ `[x]` 修自行车前拨
+ `[>]` 鸟哥的Linux私房菜： $0、1、2$ 章
+ `[x]` 数据结构作业

#### 10-03 SUN

+ `[>]` C++ Primer Plus： 第 $5、6$ 章
+ `[x]` 英语朗读

#### 10-04 MON

+ ~~`[ ]` 六级词 $\times 30$~~
+ ~~`[ ]` 四级精听 $\times 1$~~
+ ~~`[ ]` 英语朗读~~
+ `[ ]` 一刻钟学会游戏开发基础： 第 $2、3$ 节
+ `[>]` C++ Primer Plus： 第 $5、6$ 章
+ `[ ]` 鸟哥的Linux私房菜： $0、1、2$ 章

#### 10-05 TUE

+ `[x]` 数字逻辑慕课作业第 $1 \sim 2$ 章
+ `[ ]` 概率论基础教程第 $4$ 章练习
+ `[ ]` 概率论基础教程第 $5.1 \sim 5.4$ 章
+ `[x]` 大学物理实验：电桥
+ `[x]` 六级词 $\times 30$
+ ~~`[ ]` 四级精听 $\times 1$~~
+ `[>]` 考虑毛概分组
+ `[x]` 订蛋糕

#### 10-06 WED

+ `[x]` 六级词 $\times 30$
+ `[ ]` 四级精听 $\times 1$
+ `[x]` 概率论与数理统计第三章
+ `[ ]` 一刻钟学会游戏开发基础： 第 $2、3$ 节
+ `[x]` C++ Primer Plus： 第 $5、6$ 章
+ `[x]` 大学物理实验：电桥Ⅱ
+ `[x]` 数据结构大作业

#### 10-14 THU

**Note**: It is a new start maybe today. Because I would like to creating a English usage environment for me, for the reason that I seldom have a chance to speak English so that although I've pass the CET-6 English Test, it is still hard for me to communicate with others in English. So from now on, I will create chances for me to use English as much as possible. The routine I have drew up is as follow:

+ Every day before going bed at night, I will do a reflection on today's events. Different from the past, everything in my mind I will try to make it in English, which is called the English way of thinking.
+ When seeing something or doing something, I will try to describe what is it in English. For example, at this moment, I am sitting in Room 117, ZhengXin Building, which is one of the tallest structure in the campus of Harbin Institute of Technology, and studying here for free time, and the classroom is going to bu occupied later for another lesson. Therefore I have to move to the Room 207 that will be free from 10:00 today. All the details about my life will be described here every day as long as I have free time to update my bullet-journal

+ [>] Memorize 30 CET-6 vocabulary.
+ [x] Course assignment for Probability and Statistics Chapter 03.
+ [ ] ~~Music recognition research: get dataset, do MFCC analysis and DWT algorithm~~.
+ [x] Hand in Academy Physics Experiment report.
+ [>] Draw down the certificate of the Manual Project for HIT Freshman.
+ [x] C++ Primer Plus: Chapter 05 - 06.
+ [x] Mooc assignments for Logical Design Course Chapter 02 - 03.
+ [x] Logic Design: Latch and Trigger research.
+ [x] Introducing Linux by Bird Bro: Chapter: Chapter 01 - 02.

#### 10-20 WED

+ [x] Memorize 30 CET-6 vocabulary.
+ [x] Draw down the certificate of the Manual Project for HIT Freshman.
+ [x] _Game Development Learing in One Quarter of an Hour_ Mooc: _Whack A Mole_ Chapter
+ [x] HIT data structure course assignment: binary tree build, Huffman Tree build.
+ [x] Logical Design Course: Verilog and Vivado, register principles.
+ [x] C++ Primer Plus: chapter 08-11
+ [x] A First Course of Probability: Chapter 05-06

#### 10-25 MON

+ [x] Memorize 60 CET-6 vocabulary.
+ [x] C++ Primer Plus: chapter 10
+ [x] A First Course of Probability: Chapter 06
+ [x] HIT Probability Course: Chapter 04 Assignment
+ [>] HIT Logical Design MOOC Course Old: Chapter 01-02 Assignment
+ [>] Logic and Computer Design Fundamentals: Chapter 04-06
+ [>] ACM Turing Rewards PPT
+ [x] English reading aloud course assignment


#### 10-27 WED

+ [ ] HIT Logical Design MOOC Course Old: Chapter 01-02 Assignment
+ [x] Logic and Computer Design Fundamentals: Chapter 04-06
+ [x] ACM Turing Rewards PPT
+ [x] Probability: Conditional Distribution
+ [ ] Memorize 60 CET-6 vocabulary.
+ [x] C++ Primer Plus: chapter 11
+ [x] Physical Experiment: Measurement of Elasticity Modules Preview.
+ [x] Physical Experiment: Collision Targeting Report
+ [ ] Data Structure Course: Assignment

### NOVEMBER

[Future Log](#NOV)

| 日  | 一  | 二  | 三  | 四  | 五  | 六  |
|----|----|----|----|----|----|----|
|    | 1  | 2  | 3  | 4  | 5  | 6  |
| 7  | 8  | 9  | 10 | 11 | 12 | 13 |
| 14 | 15 | 16 | 17 | 18 | 19 | 20 |
| 21 | 22 | 23 | 24 | 25 | 26 | 27 |
| 28 | 29 | 30 |

+ Unity游戏开发进展：贪吃蛇，推箱子，俄罗斯方块，网格建造
+ C++ Primer Plus：看完
+ 每日六级词 $\times 30$
+ 每日六级精听 $\times 1$
+ 廖雪峰JavaScript
+ 设计模式
+ Vue.js
+ 概率论全部
+ 逻辑设计全部
+ 读完《牧羊少年》
+ 购买：
  + 深入理解计算机系统
  + 计算机网络：自顶向下方法
  + 冰球鞋

##### latest

