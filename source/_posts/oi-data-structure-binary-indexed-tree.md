---
title: 【OI考古】数据结构 | 树状数组
tags:
  - OI考古
  - 数据结构
  - C/C++
categories:
  - 编程竞赛 (Competitive Programming)
  - 数据结构
katex: true
cover: 'https://z3.ax1x.com/2021/03/31/ckF0rq.md.png'
date: 2021-04-27 16:20:52
---

树状数组或二元索引树（英语：Binary Indexed Tree，Fenwick Tree），是一种用于高效处理对一个存储数字的列表进行更新及求前缀和的数据结构。

树状数组的元素所管理的序列结构如下图所示：

![g9RY7V.md.png](https://z3.ax1x.com/2021/04/27/g9RY7V.md.png)

## 树状数组（单点修改）

### 模板题：[洛谷 P3374  | [模板] 树状数组 1](https://www.luogu.com.cn/problem/P3374)

#### 题目描述

如题，已知一个数列，你需要进行下面两种操作：

+ 将某一个数加上 $x$ 

+ 求出某区间每一个数的和

#### 输入格式

第一行包含两个正整数 $n,m$ ，分别表示该数列数字的个数和操作的总个数。

第二行包含 $n$ 个用空格分隔的整数，其中第 $i$ 个数字表示数列第 $i$ 项的初始值。

接下来 $m$ 行每行包含 $3$ 个整数，表示一个操作，具体如下：

+ `1 x k` 含义：将第 $x$ 个数加上 $k$ 

+ `2 x y` 含义：输出区间 $[x,y]$ 内每个数的和

#### 输出格式



#### 输入输出样例

##### 输入
```
5 5
1 5 4 2 3
1 1 3
2 2 5
1 3 -1
1 4 2
2 1 4
```

##### 输出
```
14
16
```

#### 说明/提示

【数据范围】 

对于 $30\%$ 的数据， $1 \le n \le 8$ ， $1\le m \le 10$ ； 

对于 $70\%$ 的数据， $1\le n,m \le 10^4$ ； 

对于 $100\%$ 的数据， $1\le n,m \le 5\times 10^5$ 。

### 解决方案

#### 代码

```cpp
#include <iostream>
#include <cstdio>
using namespace std;
const int maxn = 500500;
int n, m;
long long c[maxn];
int lowbit(int x)
{
    return x & -x;
}
void add(int x, long long k)
{
    while (x <= n)
    {
        c[x] += k;
        x += lowbit(x);
    }
}
long long query(int x, int y)
{
    long long ans1 = 0, ans2 = 0;
    x--;
    while (x >= 1)
    {
        ans1 += c[x];
        x -= lowbit(x);
    }
    while (y >= 1)
    {
        ans2 += c[y];
        y -= lowbit(y);
    }
    return ans2 - ans1;
}
int main()
{
    scanf("%d %d", &n, &m);
    for (int i = 1; i <= n; i++)
    {
        long long x;
        scanf("%lld", &x);
        add(i, x);
    }
    for (int i = 1; i <= m; i++)
    {
        int op, x, k;
        scanf("%d %d %d", &op, &x, &k);
        if (op == 1)
        {
            add(x, (long long)k);
        }
        if (op == 2)
        {
            printf("%lld\n", query(x, k));
        }
    }
    return 0;
}
```

## 树状数组（区间操作）

## 请参阅

+ https://zh.wikipedia.org/wiki/%E6%A0%91%E7%8A%B6%E6%95%B0%E7%BB%84

+ https://oi-wiki.org/ds/fenwick/