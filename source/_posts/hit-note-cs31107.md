---
title: HIT | 集合论与图论 | 课程笔记 | 2021春季
tags:
  - 私人领域
  - 课程笔记
categories:
  - 日常
katex: true
description: 哈工大-离散数学引论-课程笔记
cover: 'https://z3.ax1x.com/2021/04/29/gkLouR.md.png'
date: 2021-04-29 13:42:47
---

- [集合论](#集合论)
- [第一章 集合及其运算](#第一章-集合及其运算)
- [第二章 映射](#第二章-映射)
  - [函数的一般概念——映射](#函数的一般概念映射)
  - [抽屉原理](#抽屉原理)
  - [映射的一般性质](#映射的一般性质)
  - [映射的合成](#映射的合成)
    - [例1](#例1)
  - [逆映射](#逆映射)
  - [置换](#置换)
  - [二元和n元运算](#二元和n元运算)
  - [集合的特征函数](#集合的特征函数)
- [第三章 关系](#第三章-关系)
  - [关系的概念](#关系的概念)
  - [关系矩阵和关系图](#关系矩阵和关系图)
  - [关系的合成运算](#关系的合成运算)
  - [关系的性质](#关系的性质)
    - [自反性、反自反性、非自反且非反自反性](#自反性反自反性非自反且非反自反性)
      - [定义](#定义)
      - [举例](#举例)
      - [关系图表示](#关系图表示)
  - [关系的闭包](#关系的闭包)
    - [对称性、反对称性](#对称性反对称性)
    - [](#)
- [第四章 无穷集合及其基数](#第四章-无穷集合及其基数)
- [第五章 模糊集合论](#第五章-模糊集合论)
- [图论](#图论)
- [第六章 图的基本概念](#第六章-图的基本概念)
- [第七章 树和割集](#第七章-树和割集)
- [第八章 连通度和匹配](#第八章-连通度和匹配)
- [第九章 平面图和图的着色](#第九章-平面图和图的着色)
- [第十章 有向图](#第十章-有向图)
- [请参见](#请参见)

集合论
===

# 第一章 集合及其运算

# 第二章 映射

## 函数的一般概念——映射

## 抽屉原理

## 映射的一般性质

## 映射的合成

### 例1

设 $M$ 是一个非空集合， $\varphi : M \rightarrow M ,\ N  \subseteq M$ 。令 $𝒜 = \{  P \ | \ P \subseteq M$ 且 $N \subseteq P , \ \varphi(P) \subseteq P \}$ ， $G= \displaystyle\bigcap_{P \in 𝒜} P$ 。 试证：

1. $G \in 𝒜$ ；

2. $N \bigcup \varphi (G) = G$ 。

**证明**：

1.  $\forall x \in G ， P \in 𝒜$ ，有 $x \in P$ 

    所以 $\forall x \in G ， x \in M$ ，即 $G \subseteq M$ 

    $\forall x \in N ，x \in P$ ，即 $x \in \displaystyle\bigcap_{P \in 𝒜} P = G$ ，所以 $N \subseteq G$

    $\forall x \in G$ ，有 $\varphi (x) \in P$ ，即 $\varphi (x) \in \displaystyle\bigcap_{P \in 𝒜} P = G$ ，所以 $\varphi (G) \subseteq G$

    综上 $G \in 𝒜$

2.  
    + 先证 $N \bigcup \varphi (G) \subseteq G$ 。

      $\forall x \in N \bigcup \varphi (G)$ ， $x \in N \ 或 \ x \in \varphi (G)$

      若 $x \in N$ ，因为 $N \subseteq G$ ，所以 $x \in G$ 。

      若 $x \in \varphi (G)$ ，因为 $\varphi (G) \subseteq G$ ，所以 $x \in G$ 。

      所以 $N \bigcup \varphi (G) \subseteq G$ 。

    + 再证 $G \subseteq N \bigcup \varphi (G)$ ：

      $\forall x \in G$ ，

      由于 $N \subseteq G$ ，

      若 $x \in N$ , $x \in N \bigcup \varphi (G)$

      若 $x \notin N$ ,  则 $x \in G \backslash N$

      - 下证： $\forall x \in G \backslash N, \ \varphi (x) \notin N$ 。

        假设 $\varphi (x) \in N$ ，那么 $G \backslash x \in 𝒜$ ，与 $G= \displaystyle\bigcap_{P \in 𝒜} P$ 矛盾。

        故  $\forall x \in G \backslash N, \ \varphi (x) \notin N$ 

        即  $\forall x \in G \backslash N, \ \varphi (x) \in G \backslash N$ 
  
      - 再下证： $\varphi : G \backslash N \rightarrow G \backslash N$ 是双射。

        假设 $\varphi : G \backslash N \rightarrow G \backslash N$ 不是满射，

        即 $\exist y \in G \backslash N , \ s.t. \ \varphi ^{-1} (y) = \empty$ ，

        那么 $G \backslash y \in 𝒜$ ，与 $G= \displaystyle\bigcap_{P \in 𝒜} P$ 矛盾。

        所以 $\varphi : G \backslash N \rightarrow G \backslash N$ 是满射，也是双射。

        即 $\varphi (G \backslash N) = G \backslash N$

      所以 $x \in G \backslash N = \varphi (G \backslash N) \subseteq \varphi (G)$ 。





## 逆映射

## 置换

## 二元和n元运算

## 集合的特征函数

# 第三章 关系

## 关系的概念

## 关系矩阵和关系图

## 关系的合成运算

## 关系的性质

### 自反性、反自反性、非自反且非反自反性 

#### 定义

设 $R \sube A \times A$ :

+ $\forall x \in A$ ， $<x, x> \in R$ ，则称 $R$ 在 $A$ 上是自反的（reflexive），或称 $R$ 具有自反性（reflexivity）。

+ $\forall x \in A$ ，$<x, x> \notin R$ ，则称 $R$ 在 $A$ 上是反自反的（antireflexive），或称 $R$ 具有反自反性（antireflexivity）。

#### 举例

+ 同姓关系、小于等于关系、包含关系、整除关系是**自反**关系。

+ 父子关系，小于关系，真包含关系都是反自反关系。

#### 关系图表示

自反：全都有自环。

反自反：没有自环。

非自反且非反自反：有且仅有部分有自环。

## 关系的闭包

### 对称性、反对称性

### 



# 第四章 无穷集合及其基数

# 第五章 模糊集合论

图论
===

# 第六章 图的基本概念

# 第七章 树和割集

# 第八章 连通度和匹配

# 第九章 平面图和图的着色

# 第十章 有向图


请参见
===

+ https://zhuanlan.zhihu.com/p/57478122