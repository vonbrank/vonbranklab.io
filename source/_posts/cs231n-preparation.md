---
title: CS231n | 先决条件 Preparation
tags:
    - 教程
    - 深度学习
    - Python
    - CS231n
categories:
    - 深度学习
    - 计算机视觉
cover: 'https://z3.ax1x.com/2021/03/21/64GeH0.md.png'
date: 2021-03-21 00:51:30
---


近期在和朋友做计算机视觉相关的年度项目，根据导师的建议，我们选择了Stanford的CS231n课程作为计算机视觉的入门课程。然而由于某些原因， ~~初期的学习线路出了一些偏差，~~ 临近中期才发现CS231n官方提供了非常完善的笔记和作业文档，免去了大面积重复造轮子的工作，只需要编写部分核心代码。因此我决定重修CS231n课程，写下这一系列文章记录学习过程。

本文提供开始学习前CS231n的准备步骤，主要翻译自官方提供的[Software Setup](https://cs231n.github.io/setup-instructions/)文档，供日后查询。

## 使用Google Colaboratory工作



## 在本地设备上工作

### 配置Anaconda虚拟环境

CS231n官方推荐使用Anaconda搭建Python编程环境并管理Numpy等包及其依赖项。这样做的好处之一在于，其自带[MKL优化](https://docs.anaconda.com/mkl-optimizations/)，这可以加速`numpy`与`scipy`代码的执行。

Anaconda虚拟环境的搭建在其他文章中已做介绍，在此不再赘述。

安装完Anaconda后，需创建名为`cs231n`的虚拟环境，可以在终端中运行以下指令创建：

```bash
# 这将在'path/to/anaconda3/envs/'下创建
# 名为cs231n的Anaconda虚拟环境
conda create -n cs231n python=3.7
```

要激活并进入所创建的环境，请运行`conda activate cs231n`；要禁用并退出环境，请运行`conda deactivate cs231n`或关闭终端。需要注意的是，每次写作业前都需要重新运行`conda activate cs231n`。

### 安装包集合

激活虚拟环境后，你需要安装课程所需的Numpy，Matplotlib等库。为安装所有依赖项，你可以下先下载Assignment#1的Jupyter notebook[笔记文件](https://cs231n.github.io/assignments/2020/assignment1_jupyter.zip)，通过以下指令进入Assignment#1的文件目录，并通过`conda`或`pip`安装`requirements.txt`所列出的依赖项：

```bash
# 再次提醒, 在运行以下指令之前
# 请确保虚拟环境已被激活
cd assignment1  # 进入assignment的目录

# 安装assignment的依赖项。
# 虚拟环境激活后，pip将自动安装
# requirements.txt中列出的所有依赖项
# 并与Python进行连接
pip install -r requirements.txt
```

### 下载CIFAR-10数据集

CS231n课程围绕CIFAR-10数据集的处理进行，要获得CIFAR-10，你可以进入`assignment1`的主目录，在终端中执行以下指令下载;

```bash
cd cs231n/datasets
./get_datasets.sh
```

然而由于[众所周知的原因](https://baike.baidu.com/item/%E9%98%B2%E7%81%AB%E5%A2%99%E9%95%BF%E5%9F%8E/13015775)，这种方式通常会失败。因此，你可以到[CIFAR-10的官网](https://www.cs.toronto.edu/~kriz/cifar.html)手动下载[数据集](https://www.cs.toronto.edu/~kriz/cifar-10-python.tar.gz)，并解压到`cs231n/datasets`目录，其效果与运行上述命令一致。

### 启用IPython

CS231n官方提供的Assignment文件提供了丰富的辅助理解内容。你可以在激活`cs231n`环境后`cd`到`assignment1`主目录，运行`jupyter notebook`，此时会打开一个网页，接着转跳至IPython的文件管理页面，到此为止，我们就可以愉快地开始写CS231n课程作业了。