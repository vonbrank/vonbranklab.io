---
title: 如何将Ubuntu安装到U盘-UEFI引导
tags:
    - 教程
    - Linux
categories:
    - 教程
katex: true
cover: https://149366088.v2.pressablecdn.com/wp-content/uploads/2020/02/Focal-Fossa-wallpaper.jpg
date: 2021-02-28 23:51:08
---

现代Ubuntu操作系统的安装以[Yukko](https://yukkohzq.github.io/)的“虚拟机可以最大程度地减少对硬件的干扰”为嚆矢。滥觞于基于启动盘的Ubuntu物理机安装方案正失去它们的借鉴意义。但面对看似无垠的未来天空，我想循全新的利用VMware虚拟机将Ubuntu安装至USB存储设备的思路，好过过早地振翮.

本教程主要讨论将Ubuntu装入U盘以提升便携性的方法，由[Yukko](https://yukkohzq.github.io/)提出，收录于此以备用。

## 先决条件

### 确认你的操作系统是UEFI引导

传统MBR与UEFI的区别在此不再赘述，详细内容可以自行Google/Baidu.

确认自己的系统是MBR或UEFI启动的方法：

+ 键盘按下`Win+R`
+ 在弹出的窗口中输入`msinfo32`，按回车键确认
+ 在弹出的窗口中确认以下信息
    ![6Czly6.md.png](https://s3.ax1x.com/2021/02/28/6Czly6.md.png)
    ![6CzJTe.png](https://s3.ax1x.com/2021/02/28/6CzJTe.png)

+ 如果显示如图所示的信息，请点击右上角的×关闭此页面；若不然，恭喜你，符合执行本教程的最低要求.

### 准备一块空U盘

其实不止U盘，其他空的存储器也行，如硬盘，SD卡等。本教程将使用一块 [SanDisk-CZ410-32GB-USB3.0](https://item.jd.com/100008461371.html#crumb-wrap) U盘 

+ 下载DiskGenius并打开
+ 在右侧找到你的U盘，右键单击之
+ 如果右键弹出菜单中“转换分区表类型为GUID格式”为可选状态，请先通过此选项将你的分区表类型转换为GUID；如果不可选，可忽略此步骤
    ![6CT49U.md.png](https://s3.ax1x.com/2021/02/28/6CT49U.md.png)

+ 在右键菜单中选择”删除所有分区“，并点击界面左上角的“保存更改”。在执行此步骤前，请务必确认你选择的是所要用于安装Ubuntu的U盘
    ![6CTzge.md.png](https://s3.ax1x.com/2021/02/28/6CTzge.md.png)
    ![6CTfhT.md.png](https://s3.ax1x.com/2021/02/28/6CTfhT.md.png)

## 配置VMware虚拟机

+ 安装[VMware Workstation](https://www.vmware.com/)，详细过程在此不再赘述

+ 下载[Ubuntu](https://ubuntu.com/)镜像，本教程将使用[ubuntu-20.04.2.0-desktop-amd64.iso](https://mirror.xtom.com.hk/ubuntu-releases/20.04.2.0/ubuntu-20.04.2.0-desktop-amd64.iso)进行安装

+ 打开VMware，选择新建虚拟机，仅需在以下几步中进行特别设置，其他步骤保持默认值即可。虚拟机位置可自行设置

    ![6CTON6.md.png](https://s3.ax1x.com/2021/02/28/6CTON6.png)
    ![6CT7u9.png](https://s3.ax1x.com/2021/02/28/6CT7u9.png)
    ![6CTbH1.md.png](https://s3.ax1x.com/2021/02/28/6CTbH1.png)
    ![6CTLAx.md.png](https://s3.ax1x.com/2021/02/28/6CTLAx.png)
    
+ 创建完成后，点击“编辑虚拟机设置”，对以下几项进行特别设置
    ![6CTv9O.md.png](https://s3.ax1x.com/2021/02/28/6CTv9O.md.png)
    ![6CTojJ.md.png](https://s3.ax1x.com/2021/02/28/6CTojJ.md.png)
    ![6CTx3D.md.png](https://s3.ax1x.com/2021/02/28/6CTx3D.md.png)
    
+ 设置完成后点击开启虚拟机

## 在虚拟机内安装Ubuntu

+ （以下内容为引用）选择第一个"Ubuntu"

    ![6CTg7q.md.png](https://s3.ax1x.com/2021/02/28/6CTg7q.md.png)

    ![6CTcBn.md.png](https://s3.ax1x.com/2021/02/28/6CTcBn.md.png)

+ 在VMware顶部菜单中选择“可移动设备”，选择你的U盘，并点击“连接”，弹出的窗口中全选“确定”即可
    ![6CHpGT.md.png](https://s3.ax1x.com/2021/02/28/6CHpGT.md.png)
    
+ 在这个界面右侧往下拉，会有中文可供选择。然后选择“安装Ubuntu”

    ![6CTWNV.md.png](https://s3.ax1x.com/2021/02/28/6CTWNV.md.png)

+ 键盘布局看个人喜好

    ![6CT53F.md.png](https://s3.ax1x.com/2021/02/28/6CT53F.md.png)

+ 尽量不要联网，也不要更新，否则边装边下载很慢

    ![6CTIc4.md.png](https://s3.ax1x.com/2021/02/28/6CTIc4.md.png)

+ 接下来的步骤非常重要。在此界面选择“其他选项”

    ![6CTHBR.md.png](https://s3.ax1x.com/2021/02/28/6CTHBR.md.png)

+ 在这里可以看见一个空分区，那就是我们的U盘

    ![6CHhy4.md.png](https://s3.ax1x.com/2021/02/28/6CHhy4.md.png)

+ 我们可以看见32GB的U盘在此显示为30765MB的空间，点空闲盘符，点击+进行分区

    ![6CbpTI.md.png](https://s3.ax1x.com/2021/02/28/6CbpTI.md.png)
    
    分区如下 :
  
  + EFI：这个就是实现你双系统的原因了，这个就是用启动 ubuntu 的目录，里面会有系统的引导，这个文件其实只有几十兆，但是我们建议将其划分为 200M ,格式为EFI
    ![6Cbiff.md.png](https://s3.ax1x.com/2021/02/28/6Cbiff.png)
  + swap： **（如果你的内存RAM大于4GB，请不要划分swap分区）** 这个是 Linux 也就是 Ubuntu 的交换区目录，这个一般的大小为内存的 2 倍左右，主要是用来在电脑内存不足的情况下，系统会调用这片区域，来运行程序，我们可以将其分为 4GB。 为创建这片空间，我们只需要点击+，将文件格式选择为“交换空间”（或者"swap"）.
    ![6CbVXQ.md.png](https://s3.ax1x.com/2021/02/28/6CbVXQ.png)
  + /：这是 Linux 也就是 Ubuntu 的根目录就一个反斜杠表示，相当于windows的C盘，我们将其分为 10G，文件格式为 Ext4（根据你的磁盘空间调整，可以大一点，如果你拥有较大容量的U盘，可将其设为30-40GB，毕竟Ubuntu装软件都是默认装在根目录的）
    ![6Cb1pT.md.png](https://s3.ax1x.com/2021/02/28/6Cb1pT.png)
  + /home： **（可以不分）** 这是 Ubuntu的其他盘，相当于Windows的其他盘，所以为了让我们自己的目录大一点，剩下的全分给它，文件格式为 Ext4
    ![6Cb8cF.md.png](https://s3.ax1x.com/2021/02/28/6Cb8cF.png)
  
+ 接下来这步非常重要。检查200MB的EFI分区前的编号是多少，我的是/dev/sdb1，不同机器可能略有不同。在这个界面下方，选择安装启动下拉菜单，然后在安装启动的下拉菜单中找到EFI分区的对应编号，并选中它，选择“继续”
    ![6Cb49f.md.png](https://s3.ax1x.com/2021/02/28/6Cb49f.png)
    
+ 余下步骤一路默认即可。设置地区，键盘布局，系统用户等设定纯看个人口味

    ![6CbONq.md.png](https://s3.ax1x.com/2021/02/28/6CbONq.md.png)
    ![6CquKe.md.png](https://s3.ax1x.com/2021/02/28/6CquKe.md.png)

+ 如果你不幸连接了互联网，在skip按钮亮起时可以点skip跳过下载某些组件，这不会影响安装，但可以加快进度。你可以在系统安装完成后再下载

    ![6CLreH.md.png](https://s3.ax1x.com/2021/02/28/6CLreH.md.png)

+ 最后系统安装完成，会提示你重启，在虚拟机里通常重启不成功，可以直接弹出U盘。
    ![6COMtI.md.png](https://s3.ax1x.com/2021/02/28/6COMtI.md.png)
    
+ 关闭物理机，插上U盘，并重新开机。进入启动菜单并选择带有Ubuntu的选项，不同品牌的主板或笔记本进入启动菜单的快捷键略有不同，我这里的快捷键是F12；或进入bios将U盘设为第一启动顺序，直接启进Ubuntu。
    ![6CvHDU.md.jpg](https://s3.ax1x.com/2021/02/28/6CvHDU.md.jpg)
    ![6CvbbF.md.jpg](https://s3.ax1x.com/2021/02/28/6CvbbF.md.jpg)
+ 如果你成功进入了Ubuntu系统登录菜单，那么恭喜你，成功将Ubuntu塞进了你的便携式独立存储器，现在你可以带着它随意行动了！！！
    ![6CxA5d.md.png](https://s3.ax1x.com/2021/02/28/6CxA5d.md.png)



## 后续步骤

希望这篇教程对你有帮助，你也可以将其推荐给你希望把Ubuntu装进U盘的朋友。如果安装过程中出现了无法解决的问题，删除虚拟机，格式化U盘，并重新操作一遍即可。
    ![6CxVPA.md.png](https://s3.ax1x.com/2021/02/28/6CxVPA.md.png)

## 请参阅

+ https://www.cnblogs.com/masbay/p/10745170.html