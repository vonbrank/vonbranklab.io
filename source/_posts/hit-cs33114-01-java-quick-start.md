---
title: '课程笔记 01 - Java快速入门 | 哈工大 CS3311: JavaEE（夏季学期）'
tags:
  - Java
  - 教程
  - 课程笔记
  - Web开发
categories:
  - Java
  - HIT-C33114-课程笔记
description: 哈尔滨工业大学夏季学期「基于Java EE平台的软件开发（田英鑫老师）」课程笔记
katex: true
cover: 'https://z3.ax1x.com/2021/08/17/fIeWKf.md.png'
date: 2021-08-31 22:20:28
---


**基于Java EE平台的软件开发**（Software Development Based on Java EE Platform）是哈尔滨工业大学夏季学期限选课程。考虑到此课程容量较大，故写下此系列笔记，包含以下内容：

+ 记录课上介绍的主要内容
+ 深入浅出地对涉及到的知识做出解释
  
希望帮助上课没跟上的同学理解Java Web开发的有关内容。

本篇是系列笔记的第一部分，将介绍Java语言的最基本内容。

## 先决条件

### C语言

作为一门类C语言，学习Java前掌握C语言有助于掌握Java的流程控制，毕竟和C相比几乎一模一样。

### C++

Java是一门面向对象编程语言，掌握C++有助于理解面向对象中继承、封装、多态性等内容。当然，身在HIT的你，根据培养方案，也许你还没有学过C++，因此此先决条件不是必须的。

## Java快速入门

### Java简介

Java是一种广泛使用的计算机编程语言，拥有跨平台、面向对象、泛型编程的特性，广泛应用于企业级Web应用开发和移动应用开发。

任职于Sun微系统的詹姆斯·高斯林等人于1990年代初开发Java语言的雏形，最初被命名为Oak，目标设置在家用电器等小型系统的编程语言，应用在电视机、电话、闹钟、烤面包机等家用电器的控制和通信。由于这些智能化家电的市场需求没有预期的高，太阳计算机系统（Sun公司）放弃了该项计划。

随着1990年代互联网的发展，Sun公司看见Oak在互联网上应用的前景，于是改造了Oak，于1995年5月以Java的名称正式发布。Java伴随着互联网的迅猛发展而发展，逐渐成为重要的网络编程语言。

随着Java的发展，SUN给Java又分出了三个不同版本：

+ Java SE：Standard Edition
+ Java EE：Enterprise Edition
+ Java ME：Micro Edition

这三者的关系可用此图表示：

```
┌───────────────────────────┐
│Java EE                    │
│    ┌────────────────────┐ │
│    │Java SE             │ │
│    │    ┌─────────────┐ │ │
│    │    │   Java ME   │ │ │
│    │    └─────────────┘ │ │
│    └────────────────────┘ │
└───────────────────────────┘
```

其中，Java SE是Java的标准版，包含JVM和Java标准库；Java EE是Java的企业版，增加的大量API和库，便于开发Web应用等企业级应用，本课程的最终目标便是学会开发一款企业级Web应用；Java ME是Java针对嵌入式设备的版本，从未真正流行起来。

因此，本课程将首先学习Java SE，接着才是Java EE。

### 安装JDK

Java程序运行在JVM上，因此在开始学习之前我们要安装JDK。

等等，JDK稍微容易理解一点，它是Java开发的工具包，说白了学一门语言之前肯定是要装某种“软件”才能在本地写代码并运行，但是什么是JVM，也许在课上你并没有听懂，不过你也暂时不需要懂，看完本节，你将在[附录](#附录)找到答案，现在听我的即可。

来到[Oracle官网](https://www.oracle.com/java/technologies/javase-downloads.html)下载最新的JDK。

如图，点击图中的`JDK Download`下周JDK，按提示安装即可。

![jdk16](../../temp/img/jdk16.png)

### 配置环境变量

接着我们需要配置环境变量。

“环境变量”对于很多人来说又是一个陌生的词，不过在此你依然可以忽略其意义，看完本节后查阅[附录](#附录)。

打开系统信息面板，通常你可以通过右键桌面上的「此电脑」（它又曾叫「计算机」、「这台电脑」、「我的电脑」等），或「文件资源管理器」首页，选择「属性」；

![this-cpt-01](../../temp/img/this-cpt-01.png)
![this-cpt-02](../../temp/img/this-cpt-02.png)

然后找到「高级系统设置」->「环境变量」->「新建」，在「变量名」处填`%JAVA_HOME%`，在「变量值」处填上你的JDK安装路径，我这里是`C:\Program Files\Java\jdk-16.0.1`，然后点击「确定」

![Evironment-Variable](../../temp/img/Evironment-Variable.png)

在「系统变量」->「Path」->「新建」，在出现的输入框里填写`%JAVA_HOME%\bin`，点击「确定」，然后重启你的电脑（不重启可能触发Windows的玄学权限bug，导致需要以管理员权限运行终端才能使用`java`和`javac`命令）。

![Evironment-Variable-02](../../temp/img/Evironment-Variable-02.png)

最后，打开命令行，通常可以通过按下`Win + R`，在弹出的窗口中输入`Powershell`（推荐）或`cmd`打开终端，然后输入`java`，如果一切正常，会显示形如以下信息：

```bash
Windows PowerShell
版权所有（C） Microsoft Corporation。保留所有权利。

安装最新的 PowerShell，了解新功能和改进！https://aka.ms/PSWindows

PS D:\Users\VonBrank> java 
用法：java [options] <主类> [args...]
           （执行类）
   或  java [options] -jar <jar 文件> [args...]
           （执行 jar 文件）
   或  java [options] -m <模块>[/<主类>] [args...]
       java [options] --module <模块>[/<主类>] [args...]
           （执行模块中的主类）
   或  java [options] <源文件> [args]
           （执行单个源文件程序）

 将主类、源文件、-jar <jar 文件>、-m 或
 --module <模块>/<主类> 后的参数作为参数
 传递到主类。
 
...

```

输入`javac`将显示：

```bash
PS D:\Users\VonBrank> javac
用法: javac <options> <source files>
其中, 可能的选项包括:
  @<filename>                  从文件读取选项和文件名
  -Akey[=value]                传递给注释处理程序的选项
  --add-modules <模块>(,<模块>)*
        除了初始模块之外要解析的根模块; 如果 <module>
                为 ALL-MODULE-PATH, 则为模块路径中的所有模块。
  --boot-class-path <path>, -bootclasspath <path>
        覆盖引导类文件的位置
  --class-path <path>, -classpath <path>, -cp <path>
        指定查找用户类文件和注释处理程序的位置

...

```

如果没有，说明配置环境变量的某一步骤出错，请重新尝试配置。

### 第一行代码

至此学习Java前的所有准备工作就完成了。

#### `Hello World!!!`

在正式开始前，你需要选择一个你认为合适的文件夹作为学习Java的工作区，右键 -> 「新建」 -> 「文本文档」。将其重命名为`Hello.java`（注意在此步骤前要确保在文件资源管理器上方的「查看」 -> 「显示」 -> 「文件扩展名」为勾选状态），并打开这个文本文档，这里推荐强烈使用[Notepad++](https://github.com/notepad-plus-plus/notepad-plus-plus/releases/download/v8.1.3/npp.8.1.3.Installer.x64.exe)编辑代码，使用系统自带的记事本编辑代码可能造成的问题不在本课程解决范围内。

接着在其中输入以下代码：

```java
public class Hello {
	public static void main(String[] args) {
		System.out.println("Hello World!!!");
	}
}
```

这便是第一行代码。

如果你熟悉C语言，那么阅读这段代码就不会很吃力。和C/C++类似，Java程序也可以有一个类似`main`函数的入口。从这里开始，我们暂时只关注`main`函数后这对大括号里的内容。

类似C中的`printf`函数，Java中的输出语句可以写成`System.out.println("Hello World!!!");`，其会将双引号内的内容原封不动地输出出来，不同的是它会在输出完成后换行。

回到存放这段代码的文件夹下，在此处打开终端（有人叫它命令行，或黑黑的窗口），如果你不知道什么的终端，也不知道怎么打开，可以参考以下方法：

+ 按下`Shift`，然后在目录空白处右键 -> 「在此处打开Powershell窗口」
![open-terminal](./../../temp/img/open-terminal.png)
+ 按下`Win + R`，在弹出的窗口中输入`Powershell`，然后在终端里输入`cd <你存放Hello.java的文件路径>`，如：我将它存放在`D:\java-learning\`下，则输入`cd D:\java-learning\`，你可以在文件目录上方的地址栏复制并粘贴输入此地址。

接着输入命令`javac Hello.java`，回车后正常情况下终端应该没有任何反馈信息，并默默地换了一行，因为「没有回答就是最好的回答」，像这样：

```bash
PS D:\Users\VonBrank\Documents\code> javac Hello.java
PS D:\Users\VonBrank\Documents\code>
```

如果没有，并提示了一些错误信息，说明之前的操作有误，请重新尝试一下。

然后我们发现`Hello.java`旁边出现的一个名为`Hello.class`的文件，这时继续输入`java Hello`，会得到结果：

```bash
PS D:\Users\VonBrank\Documents\code> java Hello
Hello World!!!
```

我们的程序运行成功！这便是Java的基础输出功能，此时的你也许和你的第一个C程序运行输出`Hello World!!!`时一样激动。

#### 发生了什么？

我们都知道，C语言是一种编译型语言，当写出一段C语言代码后，编译器直接编译成二进制文件，这是可以由计算机直接执行的机器语言。

Java则略有不同，写好一段Java代码，如上面的`Hello.java`，我们运行`javac`命令将其转化成一种`.class`类型的文件，同时为了运行`Hello.class`，Java构建了一种运行这种文件的专用程序——Java虚拟机（JVM），我们使用`java Hello`就能启动JVM，同时它能自动得知`Hello`指的就是`Hello.class`，接着便是在JVM中执行我们刚才写好的程序，输出`Hello World!!!`。

### 变量和数据类型

#### 打印变量

正如之前所说，初学Java，我们的注意力应该放在Java编程的一些基本内容，而不是高级的东西，因此再次重申在前期的内容中注意力应放在`main`函数后的大括号内，如下图所示：

```java
public class Hello {
	public static void main(String[] args) {
                //注意力应该放在这个区域

	}
}
```

接着我们来看一下Java的变量和数据类型。同样类似C/C++以及其他编程语言，Java拥有大致一样的变量类型，关于变量的概念，赋值等概念，本文将不再赘述，如有疑问，请重修C语言课程（（

创建一个新文件，命名为`DataTypeTest.java`，先输入一下代码：

```java
public class DataTypeTest {
	public static void main(String[] args) {
                

	}
}
```

虽然先前强调暂时无需把注意力放在`main`函数之外，对于C语言来说这没什么不妥，但对于Java语言，需要额外注意保持首行`public class`后的名称与文件名相同，至于原因...你会知道的。

然后在`main`函数内输入`int n = 100;`，然后将其输出，像下面这样：

```java
public class DataTypeTest {
	public static void main(String[] args) {
                int n = 100;
                System.out.println("n = " + n);
	}
}
```

接着重复之前的操作，依次输入命令`javac DataTypeTest.java`和`java DataTypeTest`，可以得到如下结果

```bash
n = 100
```

如果觉得`System.out.println`不习惯，你还可以使用类似C语言中`printf`函数的方法来格式化字符串，写成`System.out.printf("n = %d", n);`，和C语言中的写法几乎完全一致。

#### 基本数据类型

和C语言有类似写法的语言我们都称之为“类C语言”，Java就是一种“类C语言”，变量的定义、运算，以及基本的流程控制等都和C语言高度相似。

先来看基本数据类型，Java有八种基本数据类型：

+ 整数类型：byte，short，int，long
+ 浮点数类型：float，double
+ 字符类型：char
+ 布尔类型：boolean

关于数据类型的存储，反码、补码等相关知识在此不再赘述，反正和C语言差不多。

```java
int a = 123;
double b = 12.3;

```

#### 运算

加、减、乘、除、模运算运算：

```java
int a = 1234;
int b = 56;
int c;
c = a + b;      //1290
c = a - b;      //1178
c = a * b;      //69104
c = a / b;      //22，向下取整
c = a % b;      //2
```



### 字符串与数组



### 流程控制


### 使用IDE



## 附录



## 参考文献