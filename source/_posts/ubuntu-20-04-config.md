---
title: Ubuntu 20.04 配置指南
date: 2021-03-04 12:15:25
tags:
    - 教程
    - Linux
categories:
    - 教程
cover: https://149366088.v2.pressablecdn.com/wp-content/uploads/2020/02/Focal-Fossa-wallpaper.jpg
---


最近遇到的项目需要在Linux环境下进行 ~~（因为WIndows版openpose的Python接口是坏的）~~ ，由于各类Linux发行版不像Windows那样可以开箱即用，因此写下这篇文章记录Ubuntu 20.04下各种软件和环境的配置过程，方便个人查阅。本文将持续更新。

---

## System

### 更换镜像源

Linux下的软件通常使用包管理器进行安装，Ubuntu默认的软件镜像源在国外，下载速度经常慢得离谱，因此我们需要更换为国内的镜像源。

我们可以在「设置」-> 「关于」->「软件和更新」->「下载自」->「其他站点」选择一个国内的镜像源，阿里源，淘宝源，清华源都是不错的选择。

![6nSyGt.md.png](https://s3.ax1x.com/2021/03/06/6nSyGt.md.png)



### 安装显卡驱动（NVIDIA）

本人使用的图形处理器是NVIDIA Geforce RTX 2060，然而我们会发现，在「设置」-> 「关于」中「图形」这一栏显示的不是我们的GPU。这是因为Ubuntu默认使用开源的显卡驱动，而非官方驱动，在性能和功能方面还是有些区别，而且无法部署CUDA环境，因此我们还是需要安装官方驱动 ~~（顺便打打游戏）~~  。

![6npiQK.md.png](https://s3.ax1x.com/2021/03/06/6npiQK.md.png)

这里提供一种 **较简单** 的安装驱动操作。

+ 在刚才的界面打开「软件和更新」，然后选择「附加驱动」，在加载出来的驱动列表中选择合适的驱动，然后点击「应用更改」。

![6n9ovV.md.png](https://s3.ax1x.com/2021/03/06/6n9ovV.md.png)

+ 安装完成之后，系统会要求重启以加载驱动。

![6nCZ8I.png](https://s3.ax1x.com/2021/03/06/6nCZ8I.png)

+ 在此之前，会要求你设置密码，记得勾上前面的勾。

![6nCmxP.png](https://s3.ax1x.com/2021/03/06/6nCmxP.png)

+ 如果你图省事，设为“88888888”即可。

![6nCMqS.md.png](https://s3.ax1x.com/2021/03/06/6nCMqS.md.png)

+ 重新启动系统，会进入蓝色背景的Perform Mok Management界面，选择「Eroll Mok」。

![6nPuWR.md.jpg](https://s3.ax1x.com/2021/03/06/6nPuWR.md.jpg)

+ 进入Eroll Mok界面，选择「Continue」。

![6nPnY9.md.jpg](https://s3.ax1x.com/2021/03/06/6nPnY9.md.jpg)

+ 进入Eroll the Key界面，选择「Yes」。

![6nPmFJ.md.jpg](https://s3.ax1x.com/2021/03/06/6nPmFJ.md.jpg)

+ 输入安装驱动时设置的密码。

![6nP1OK.md.jpg](https://s3.ax1x.com/2021/03/06/6nP1OK.md.jpg)

+ 跳回蓝色背景的Perform Mok Management界面，这次选择第一个「Reboot」

![6nPMS1.md.jpg](https://s3.ax1x.com/2021/03/06/6nPMS1.md.jpg)

正常情况下重启之后NVIDIA显卡驱动就成功安装并加载了，可以在终端里输入以下指令验证：

```bash
nvidia-smi
```

![6nPh60.md.png](https://s3.ax1x.com/2021/03/06/6nPh60.md.png)

如果可以成功启动Ubuntu，进入图形界面，输入`nvidia-smi`却提示错误信息，可能是因为没有Disabale掉Secure Boot，可以在终端输入以下命令：

```bash
sudo mokutil --disable-validation
```

然后重新启动，在蓝色界面按下任意键，在出现的菜单中，选择第二项改变Secure Boot状态，Disable之，然后boot即可。

如果之后想重新Enable Secure Boot，只需在终端输入：

```bash
sudo mokutil --enable-validation
```

reboot并重复上述操作，把Secure Boot State改为Enable即可。

回到「设置」-> 「关于」，可以看到，我们的「图形」项已经显示正确了。

![6nPbtJ.md.png](https://s3.ax1x.com/2021/03/06/6nPbtJ.md.png)

附上实时监测CPU/GPU信息的方法：

+ CPU：使用htop工具。

  ```bash
  $ sudo apt-get install htop
  $ htop
  ```
  
+ GPU：

  ```bash
  $ watch -n 1 nvidia-smi
  ```


### 安装与配置Python

Ubuntu自带的是Python 3.x ，但某些情况下我们可能还需要使用Python 2.x ，可自行安装：

 ```bash
sudo apt install python
 ```

但是默认情况下要使用Python 2.x就必须输入`python`，要使用Python 3.x就必须输入`python3`，我们可以通过以下方法更换Python默认版本：
+ 设置python版本：

  ```bash
  sudo update-alternatives --install /usr/bin/python python /usr/bin/python2 100
  sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 150
  ```

+ 切换python版本：
  ```bash
  sudo update-alternatives --config python
  ```

+ 按照提示输入编号即可。

### 安装与配置Git

 ```bash
apt install git
 ```

全局初始化用户名和邮箱：

 ```bash
git config --global user.name "你的用户名"
git config --global user.email "你的邮箱"
 ```

用以下命令确认是否输入正确：

 ```bash
git config --list 
 ```

输入以下命令开始进行Git与Github的绑定操作：

 ```bash
 ssh-keygen -t rsa -C "你的邮箱"
 ```

在网页版Github「Settings」->「SSH and GPG keys」中New一个SSH key，然后在`~\.ssh`目录下找到`id_rsa.pub`，复制里面的文本到刚才New SSH key界面的输入框中，点击「Add SSH key」完成绑定。

在终端中输入以下指令验证是否绑定成功：

  ```bash
ssh -T git@github.com
  ```

如果返回类似如下信息，即为绑定成功：
 ```
Hi vonbrank! You've successfully authenticated, but GitHub does not provide shell access.
 ```

## Software

### 安装与配置Clash

clash是在Windows/MacOS/Linux等多平台使用的代理工具，Windows下的clash可以开箱即用，Linux下需要手动配置，流程如下：

+ 下载clash-Linux[安装包](https://github.com/Dreamacro/clash/releases)：

![6KrJsg.md.png](https://s3.ax1x.com/2021/03/07/6KrJsg.md.png)

+ 在你喜欢的位置创建一个目录，将安装包放进去，可以修改成一个易于识别的名字，如`clash.gz`，打开终端输入以下指令解压：

  ```bash
gunzip clash.gz
  ```

+ 给予文件执行权限：

  ```bash
chmod +x clash
  ```

+ 启动文件

  ```bash
./clash
  ```

+ 首次启动时clash会在`~/.config/clash`下自动生成两个文件：Config.yaml 和Country.mmdb。接着关闭clash，开始配置。

+ Country.mmdb为全球IP库，可以实现各个国家的IP信息解析和地理定位，没有这个文件clash无法运行。然而自动生成的文件无法支持clash运行，此处直接提供它的[下载地址](https://wws.lanzous.com/isEePmmiwaj)。

+ 编辑Config.yaml文件，其内容为服务器节点等信息，部分服务提供商会提供.yml文件，将其后缀修改为.yaml即可。对于提供订阅链接的服务商，可用以下命令生成Config.yaml文件：

  ```bash
wget -O Config.yaml {clash订阅地址}
  ```

+ 将得到的Config.yaml 和Country.mmdb文件复制到`~/.config/clash`目录下替换即可。

+ 再次启动clash:

  ```bash
./clash
  ```

+ 接着在「设置」->「网络」->「网络代理」,选择「手动」
  将HTTP代理设为: `127.0.0.1:7890`
  将Socks主机设为: `127.0.0.1:7891`

![6KsXB4.md.png](https://s3.ax1x.com/2021/03/07/6KsXB4.md.png)

+ 打开浏览器，访问 [http://clash.razord.top](http://clash.razord.top) 进行节点选择等操作。
+ 到此为止clash就配置完成了，你可以通过访问 [https://www.google.com/](https://www.google.com/) 来检验是否配置成功



然而我们目前只能让clash代理浏览器，终端内的`git`，`curl`，`wget`等指令仍然不走clash代理，因此用Git来push代码或用wget来下载海外服务器上的文件仍然很慢，我们可以用 **proxychains4** 来无脑设置代理。

+ 安装proxychains4：

  ```bash
sudo apt install proxychains4
  ```

+ 配置代理信息：

  ```bash
sudo nano /etc/proxychains4.conf
  ```

+ 在最后一行加上clash的代理端口：

  ```bash
socks5 127.0.0.1 7891
  ```

然后就可以用proxychains4来代理`git`，`curl`，`wget`等指令了，使用时只需要在这些指令前加上`proxychains4`即可：

 ```bash
proxychains4 git clone {远程仓库地址}
 ```

### 安装Typora

### Hexo多终端工作

---

## Tips

### 由Linux直接进入bios

很多时候我们希望重启之后直接进入bios，而不是疯狂按快捷键，可执行以下指令：

 ```bash
sudo systemctl reboot --firmware-setup
 ```


## Issues

### Windows、Ubuntu 双系统时间不统一（引用）

由于Windows 与类 Unix 系统(Unix/Linux/Mac)看待系统硬件时间的方式是不一样的：

+ Windows 把计算机硬件时间当作本地时间(local time)，所以在 Windows 系统中显示的时间跟 BIOS 中显示的时间是一样的。

+ 类 Unix 系统把计算机硬件时间当作 UTC， 所以系统启动后会在该时间的基础上，加上电脑设置的时区数(比中国就加8)，因此 Ubuntu 中显示的时间总是比 Windows 中显示的时间快 8 小时。

当你在 Ubuntu 中把系统显示的时间设置正确后，计算机硬件时间就变成了在这个时间上减去 8 小时，所以当你切换成 Windows 系统后慢了8小时。

**解决方案** ：在 Ubuntu 中把计算机硬件的时间改成系统显示时间，即禁用 Ubuntu 中的 UTC。在终端输入以下命令：

 ```bash
timedatectl set-local-rtc 1 --adjust-system-clock
 ```

## 请参阅

+ https://juejin.cn/post/6844904149822210056#heading-0
+ https://blog.csdn.net/sinat_37781304/article/details/82729029#t27
+ https://www.cnblogs.com/sueyyyy/p/12424178.html