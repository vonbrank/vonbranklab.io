---
title: HIT | 工科数学分析 | 课程笔记 | 2021春季
tags:
  - 私人领域
  - 课程笔记
categories:
  - 日常
katex: true
# password: ""
description: 哈工大-微积分B-课程笔记
cover: 'https://z3.ax1x.com/2021/04/27/g956YV.md.png'
date: 2021-04-27 15:35:21
---



# 第八章 多元函数微分学

## 二重极限

## 偏导数

## 全微分

 $$z=f(x, y)$$

 $$dz =  \frac {\partial f} {\partial x} dx + \frac {\partial f} {\partial y} dy$$

## 复合函数的链式法则

## 隐函数求导法

## 多元微分学在几何中的应用

## 二元函数的极值

## 方向导数与梯度

## 例题

# 第九章 多元函数积分学

## 黎曼积分

## 二重积分

## 三重积分

### 球坐标系下三重积分举例

#### 例1

计算 $\displaystyle\iiint_{\Omega} \sqrt{x^2 + y^2 + z^2} d \Omega$，其中 $\Omega$ 是由球面 $x^2 + y^2 + z^2 = z$ 所围。  

**解**： 球面化为球坐标得，

  $$\rho^2 = \rho cos \phi$$ 

  积分域:

  $$\Omega\begin{cases} 0 \le \theta \le 2\pi \\ 0 \le \phi \le \pi / 2 \\ 0 \le \rho \le cos \phi
  \end{cases}$$

原式：

 $$\begin{aligned}

I 
 
& = \int _0 ^{2\pi} d \theta \int _0 ^{\pi / 2} d \phi \int _0 ^{\cos \phi} \rho ^3 \sin \phi d \rho \\

& = 2 \pi \int _0 ^{\pi / 2} \sin \phi  d \phi \int _0 ^{\cos \phi} \rho ^3 d \rho \\

& = \frac{2 \pi}{4}  \int _0 ^{\pi / 2} sin \phi \cos ^4 \phi \\

& = \frac{\pi}{10}

 \end{aligned}$$

#### 例2



## 第一型曲线积分

### 定义

若 $C$ 是空间或平面一有限曲线段， $f(x, y, z)$ $\large($ $f(x, y)$ $\large)$ 是 $C$ 上的一个连续函数，则 $$ 是 $f$ 在 $C$ 上的第一型曲线积分。

## 第一型曲面积分

## 黎曼积分的应用

## 例题

# 索引

## 名词

## 符号

### 运算符

#### 大型运算符

 $\sum ， \int ，\iint  ，\iiint  ，\oint  ，  ，  ，$

 $\int _{a} ^{b} x dx$


### Style, Color, Size, and Font

#### Style

 $\displaystyle\sum_{i=1}^n，\textstyle\sum_{i=1}^n$ 

