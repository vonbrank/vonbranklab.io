---
title: C语言答疑-04
date: 2020-11-23 18:30:00
tags: 
    - C/C++
    - 教程
    - 课程笔记
categories:
    - C/C++
    - HIT-CS31106-课程答疑
description: C语言答疑-04 | Audience-120L0213 | By Von Brank
katex: true
cover: https://s3.ax1x.com/2021/02/28/698AeS.md.png
---

# C语言答疑-04

​		By Von Brank | 2020/11/23



## 目录

> + 网站推荐
> + 近期知识梳理
> + 几道题
> + 答疑



[![DJSO81.md.jpg](https://s3.ax1x.com/2020/11/23/DJSO81.md.jpg)](https://imgchr.com/i/DJSO81)



## 网站推荐

+ 算法可视化：visualgo.net/zh
+ 算法学习：oi-wiki.org



## 知识点梳理

### 指针

#### `&`：取地址的运算

+ `scanf("%d", &n);`里的`&`是什么

+ `%p`用来输出地址

  ```cpp
  int i = 0;
  printf("0x%x\n", &i);
  ```

  ```cpp
  int i = 0;
  int p;
  int p = (int)&i;
  printf("0x%x\n", p);
  printf("%p\n", &i);
  ```

+ 32位与64位的区别

  ```cpp
  int i = 0;
  printf("%lu\n", sizeof(int));
  printf("%lu\n", sizeof(&i));
  ```

+ `&`可以用于获得变量的地址，它的操作数必须是变量

+ 不能对没有地址的东西取地址

  以下写法是正确的

  ```cpp
  int i = 0;
  printf("%p\n", &i);
  ```

  以下写法都是错误的

  ```cpp
  int a = 0, b = 1;
  printf("%p\n", &(a++));
  printf("%p\n", &(++a));
  printf("%p\n", &(a + b));
  ```

#### `&`些别的

+ 相邻变量的地址

  ```cpp
  int a = 0, b = 1;
  printf("%p\n%p\n", &a, &b);
  ```

  C语言的变量存储在“栈”中
  
+ `sizeof`一个`&`的结果

  ```cpp
  int i = 0;
  printf("%d\n", sizeof(&a));
  ```

+ 数组的地址，首个数组单元的地址，相邻元素的地址

  ```cpp
  int a[100];
  printf("%p\n", a);
  printf("%p\n", &a[0]);
  printf("%p\n", &a[1]);
  printf("%p\n", &a[2]);
  ```

  

#### 指针变量

+ `scanf("%d", &n)`是什么

  + `scanf`函数需要传入一个变量的地址，便于赋值
  + `scanf`函数需要对控制串进行处理
  + `scanf`函数的内部实现也仅仅是基于C语言的基本语法

+ 指针变量的定义

  以下代码将`i`的地址赋给`p`

  ```cpp
  int i;
  int *p = &i;
  ```

  以下两种写法意思相同

  ```cpp
  int *p;
  int* p;
  ```

  以下写法定义了一个指针变量`p`和一个`int`类型的变量`q`

  ```cpp
  int *p, q;
  ```

  以下写法定义了两个指针变量`p`和`q`

  ```cpp
  int *p, *q;
  ```

+ 访问地址上的变量*

  ```cpp
  int i = 10;
  int *p = &i;
  printf("%d\n", i);
  printf("%d\n", *p);
  *p = 20;
  printf("%d\n", i);
  printf("%d\n", *p);
  ```

+ 指针作为函数参数传入

  比较函数`f`和`g`的输出差异

  ```cpp
  #include <stdio>
  void f(int *p)
  {
      printf("%d\n", *p);
      printf("%p\n", p);
  }
  void g(int k)
  {
      printf("%d\n", k);
      printf("%p\n", &k);
  }
  int main()
  {
      int i = 10;
      f(&i);
      g(i);
      return 0;
  }
  ```

+ `&`与`*`互为反作用

  ```cpp
  int i = 10;
  int *p = &i;
  printf("%d\n", i);
  printf("%d\n", *&i);
  printf("%p\n", p);
  printf("%p\n", &*p);
  ```

+ `scanf("%d", i);`出错的原因

#### 指针与数组

+ 以下两段代码效果相同

  ```cpp
  #include <stdio.h>
  
  void f(int a[], int start, int end)
  {
      for(int i=start; i<=end; i++)
      {
          printf("%d ", a[i]);
      }
  }
  
  int main()
  {
      int start, end;
      int a[100] = {0, 2, 3, 5, 7, 11, 13, 17, 19, 23, 29};
      start = 1;
      end = 10;
      f(a, start, end);
      return 0;
  }
  ```

  ```cpp
  #include <stdio.h>
  
  void f(int *a, int start, int end)
  {
      for(int i=start; i<=end; i++)
      {
          printf("%d ", a[i]);
      }
  }
  
  int main()
  {
      int start, end;
      int a[100] = {0, 2, 3, 5, 7, 11, 13, 17, 19, 23, 29};
      start = 1;
      end = 10;
      f(a, start, end);
      return 0;
  }
  ```

+ 以下函数**原型**等价

  ```cpp
  int sum(int *p, int i);
  int sum(int *, int i);
  int sum(int a[], int i);
  int sum(int [], int i);
  ```

+ 数组变量本身就是特殊的指针

  以下操作无需取地址符

  ```cpp
  int a[100];
  int *p = a;
  ```

+ 数组的单元表达的是变量，需要取地址，如：

  ```cpp
  if(a == &a[0])
  {
      printf("YES!!!");
  }
  ```

+ `[]`运算符也可以用于指针，如

  ```cpp
  int a = 10;
  int *p = &a;
  printf("%d\n", *p);
  printf("%d\n", p[0]);
  ```

+ `*`也可以用于数组

  ```cpp
  int a[100] = {0, 2, 3, 5, 7, 11, 13, 17, 19, 23, 29};
  a[0] = 123;
  printf("%d\n", a[0]);
  printf("%d\n", *a);
  *a = 456;
  printf("%d\n", a[0]);
  printf("%d\n", *a);
  ```

+ 数组变量是常量（const），即`int a[]`等价于`int * const a`，因此：

  以下写法是错误的

  ```cpp
  int a = {0, 1, 2, 3};
  int b[] = a;
  ```

  以下写法是正确的

  ```cpp
  int a = {0, 1, 2, 3};
  int *p = a;
  ```

#### const指针

+ `int * const p = &i;`的含义

  表示指针变量`p`只能指向`i`的地址

  ```cpp
  int i, j;
  int * const p = &i;
  *p = 123; //可以
  p = &j; //不可以
  ```

+ `const int *p = &i`的含义

  表示不允许通过`*p`来修改变量的值

  ```cpp
  int i, j;
  const int *p = &i;
  *p = 123; //不可以
  i = 456; //可以
  p = &j; //可以
  ```

+ 分辨以下写法的区别

  + `const int* p = &i;`表示不能通过`*p`来修改变量的值
  + `int const* p = &i;`表示不能通过`*p`来修改变量的值
  + `int *const p = &i;`表示不能改变指针变量`p`指向的变量

+ 可以用于防止函数修改指针所指向变量的值或数组的值，保证安全性

  ```cpp
  #include <stdio.h>
  void f(const int* a)
  {
      int b = *a; //可以
      *a = 123; //不可以
  }
  ```

+ 常量数组

  `const`类型的数组只能使用集成初始化

  ```cpp
  const int a[] = {0, 1, 2, 3, 4, 5};
  ```

#### 指针的运算

+ 对于一个指针变量`p`，`p + 1`的含义不是让指针的值`+1`，而是加上`sizeof(<指针变量指向的变量类型>)`

  ```cpp
  char s[] = "hello_world";
  char *p = s;
  printf("%p\n", p);
  printf("%p\n", p + 1);
  int a[] = {0, 1, 2, 3, 4, 5, 6};
  int *q = a;
  printf("%p\n", q);
  printf("%p\n", q + 1);
  ```

  ```cpp
  char s[] = "hello_world";
  char *p = s;
  printf("%c\n", *p);
  printf("%c\n", *(p + 1));
  int a[] = {0, 1, 2, 3, 4, 5, 6};
  int *q = a;
  printf("%d\n", *q);
  printf("%d\n", *(q + 1));
  ```

  ```cpp
  int a[105];
  for(int i=1; i<=100; i++)
  {
      scanf("%d", a + i);
  }
  for(int i=1; i<=100; i++)
  {
      printf("%d", a + i);
  }
  ```

  `a[i]`和`*(a + i)`完全等价

+ 指针变量可以进行加减，但指针的乘除没有意义
  ```cpp
  int a[100];
  int *p1 = a;
  int *p2 = &a[7];
  printf("p1 = %p\n", p1);
  printf("p2 = %p\n", p2);
  printf("p2 - p1 = %d\n", p2 - p1);
  ```

  `*p++`表示取出`p`指向的那个数据，然后将`p`移动到下一个位置
  
  ```cpp
  int a[] = {0, 1, 2, 3, 4, 5, 6, 7, -1};
  int *p = a;
  while(*p != -1)
  {
      printf("%d ", *p++);
  }
  ```

#### 指针使用的其他注意事项

+ 所有指针的大小都是一样的，32位程序的指针是4字节，64位是8字节

+ 为了防止使用出错，指向不同数据类型的指针不能相互赋值，如：

  ```cpp
  int *p = &n;
  char *q = &c;
  p = q;
  ```

+ `void*`表示指向未知类型的指针，编译器不知道它指向的是什么数据类型

  ```cpp
  int *p1 = &n;
  char *p2 = &c;
  void *p3;
  p3 = p1;
  p3 = p2;
  ```

+ 指针也可以做强制类型转换

  ```cpp
  int *p = &n;
  char *q = &c;
  p = (void*)q;
  ```

+ 指针究竟有什么用

  + 便于向函数中传入大量数据，如数组作为参数传入
  + 将数组传入函数后对数组作操作
  + 函数需要返回一个数组
  + 用函数修改外部变量的值
  + 动态内存分配



## 几道题

关于指针的题没几道，关于链表的倒是有不少（bushi