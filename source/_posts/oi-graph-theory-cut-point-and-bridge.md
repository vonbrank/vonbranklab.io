---
title: 【OI考古】图论 | 割点和桥
tags:
  - OI考古
  - 图论
  - C/C++
categories:
  - 编程竞赛 (Competitive Programming)
  - 图论
katex: true
cover: 'https://z3.ax1x.com/2021/03/23/6HVwkD.md.png'
date: 2021-05-30 22:24:07
---


## 割点

对于一个无向图 $G$ ，如果把一个点 $u$ 删除后这个图的极大连通分量数增加了，那么这个点就是这个图的割点（又称割顶）。

### 模板题： [洛谷 P3388 - [模板]割点(割顶)](https://www.luogu.com.cn/problem/P3388)

#### 题目描述


给出一个 $n$ 个点， $m$ 条边的无向图，求图的割点。

#### 输入格式


第一行输入两个正整数 $n,m$ 。

下面 $m$ 行每行输入两个正整数 $x,y$ 表示 $x$ 到 $y$ 有一条边。

#### 输出格式

第一行输出割点个数。

第二行按照节点编号从小到大输出节点，用空格隔开。

#### 输入输出样例

##### 输入
```
6 7
1 2
1 3
1 4
2 5
3 5
4 5
5 6
```

##### 输出
```
1 
5
```

#### 说明/提示

对于全部数据， $1\leq n \le 2\times 10^4$ ， $1\leq m \le 1 \times 10^5$ 。

点的编号均大于 $0$ 小于等于 $n$ 。

**tarjan图不一定联通。**

## Tarjan算法求割点

解决这类连通性相关的图论问题通常都要请出Tarjan算法。关于Tarjan算法的基本实现思路可以参考<a href="{% post_path oi-graph-theory-strongly-connected-components %}#DFS生成树">这里</a>。


### 思路

在无向图中使用Tarjan算法遍历图时，若对于某个点 $u$ ，若有`dfn[u]=low[u]`，说明从 $u$ 向下DFS无法回到其祖先节点，这意味着以 $u$ 为根的DFS生成子树下的所有节点和 $u$ 的祖先节点之间的路全都经过 $u$ ，若去掉 $u$ 则图将不连通，说明 $u$ 就是割点。

### 解决方案

```cpp
#include<iostream>
#include<cstdio>
#include<algorithm>
#include<cstdlib>
#include<cstring>
using namespace std;
const int maxn = 100500;
int n, m, cnt = -1, cc;
int head[maxn], dfn[maxn], low[maxn], fa[maxn], sum;
bool ans[maxn]; 
struct Node
{
	int to, next;
} edge[maxn*2];
void addedge(int u, int v)
{
	++cnt;
	edge[cnt].to = v;
	edge[cnt].next = head[u];
	head[u] = cnt;
}
void tarjan(int u)
{
	int son = 0, root;
	++cc;
	dfn[u] = cc;
	low[u] = cc;
	for(int i=head[u]; ~i; i=edge[i].next)
	{
		int v = edge[i].to;
		if(fa[u]!=v)
		{
			if(!dfn[v])
			{
				fa[v] = u;
				tarjan(v);
				low[u] = min(low[u], low[v]);
				if(low[v]>=dfn[u])
				{
					ans[u] = true;
					if(fa[u]==u)
					{
						root = u;
						son++;
					}
				}
			}
			else low[u] = min(low[u], dfn[v]);
		}
	}
	if(son==1) ans[root] = false;
}
int main()
{
	scanf("%d %d", &n, &m);
	memset(head, -1, sizeof(head));
	for(int i=1; i<=m; i++)
	{
		int u, v;
		scanf("%d %d", &u, &v);
		fa[u] = u;
		fa[v] = v;
		addedge(u, v);
		addedge(v, u);
	}
	for(int i=1; i<=n; i++)
	{
		if(!dfn[i]) tarjan(i);
	}
	for(int i=1; i<=n; i++)
	{
		if(ans[i]) sum++;
	}
	printf("%d\n", sum);
	for(int i=1; i<=n; i++)
	{
		if(ans[i]) printf("%d ", i);
	}
	return 0;
}
```

## 请参见

+ https://oi-wiki.org/graph/cut/
+ https://www.cnblogs.com/letisl/p/12243273.html#%E5%89%B2%E7%82%B9%E4%B8%8E%E6%A1%A5