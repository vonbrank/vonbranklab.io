---
title: Anaconda部署与使用指南（Windows）
tags:
    - 教程
    - Windows
    - Python
categories:
    - 教程
date: 2021-03-13 20:30:54
cover: https://s3.ax1x.com/2021/03/13/6w8xDP.md.png
---




这几天在计划在重修 ~~(bushi)~~ Stanford University的计算机视觉基础课[CS231n](http://cs231n.stanford.edu/)，在其课程作业（Assignments）中推荐使用Anaconda为课程创建虚拟环境以完成作业。本文将简要介绍Anaconda在Windows下的使用方法。



## 先决条件

对于许多Python初学者（比如我）来说，也许难以理解为什么需要需要像Anaconda这样的虚拟环境工具，因此会对项目中涉及此类工具的环节和操作感到一头雾水。因此在了解Anaconda的使用方法前，应先理解其存在的目的。

### Python解释器

初学Python的人大都有过这样的经历：

+ 依照教程到Python官网下载某个特定的安装包
+ 安装时记得勾选将Python设为环境变量的选项
+ 然后打开控制台，输入`python`，出现以下信息即表明Python安装成功

  ![6dvr8I.md.png](https://s3.ax1x.com/2021/03/13/6dvr8I.md.png)
  
+ 接着就可以在各类ide或文本编辑器愉快地写代码了

过了不久，我们会知道，某些代码运行在Python 2.x上，与Python 3.x不兼容，需要再安装一个Python 2.x的解释器，这时需要想办法使两款解释器共存。问题不难解决，只是在切换环境变量比较麻烦而已。

### Python中的包

Python中的包可以认为是一些模块的集合，所有包又组成一个包集合。我们会接触一些常见的包，如科学计算库Numpy，绘图库Matplotlib。每个Python程序都需要依赖特定的Python版本和Python包。在同一个环境中编写基于不同Python解释器的程序时的环境切换问题尚且能够解决，但不同程序所依赖的包往往不同，意味着所有程序的包都需要安装到同一个系统环境下，导致环境混乱，此外，在不同终端开发同一程序又面临包集合的迁移问题，非常麻烦。

### Anaconda

Anaconda正是为了解决上述问题而产生，其通过创建虚拟环境，将每个Python程序所需的独有开发环境，即对应的解释器，所需的包集合打包出来，需要开发某一程序时只需要切换到对应的环境即可，非常优雅。



## Anaconda 安装

推荐在[官网](https://www.anaconda.com/products/individual)下载Anaconda，首页底部的Python 3.8版本的Anaconda就不错。

安装过程中，除了这一步需要看个人需求选择是否将Anaconda添加至环境变量外（我是全勾上了），其他都为默认值即可。

![6wKxBD.png](https://s3.ax1x.com/2021/03/13/6wKxBD.png)

安装完成之后，在「开始」菜单中可以看见多了不少项，你可以直接在PowerShell、Windows Terminal等系统自带终端中使用Anaconda，但推荐使用Anaconda PowerShell Prompt，打开之后也是类似终端的操作，事实上你可以在Windows Terminal的配置文件中添加一下项来在Windows Terminal中使用Anaconda：

```json
{
    // Make changes here to the cmd.exe profile.
    "guid": "{0A9CC558-3024-89F3-5313-2C72A1789FEA}",
    "name": "Anaconda Prompt Powershell",
    "commandline": "%windir%\\System32\\WindowsPowerShell\\v1.0\\powershell.exe -ExecutionPolicy ByPass -NoExit -Command {你的Anaconda安装路径}\\shell\\condabin\\conda-hook.ps1; conda activate;",
    "icon": "{你的Anaconda安装路径}\\Menu\\anaconda-navigator.ico",
    "hidden": false
},
```

## Anaconda 使用

打开Anaconda PowerShell Prompt，输入 `conda activate`，可以看见路径前面多了一个`(base)`，表明我们现在正处于Anaconda的默认环境中。

![6w1LIe.md.png](https://s3.ax1x.com/2021/03/13/6w1LIe.md.png)



在开始之前，可以先修改一下镜像源，这里我们使用清华源：

```bash
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/
conda config --set show_channel_urls yes
```

然后更新一下所有工具包来避免可能发生的错误：

```bash
conda upgrade --all
```

### 创建与管理虚拟环境和包

base环境是Anaconda为我们准备的默认配置，你也可以创建属于自己的环境，输入以下命令创建一个名为`test`的环境：

```base
conda create -n test python=3
```

其中`python=3`表示指定本环境所用的Python版本为Python 3.x的最新版本。若需进一步指定版本，如指定Python3.6，则可输入`python=3.7`；若需使用Python 2.x，只需改为`python=2`即可。

接着通过下面的指令切换到我们想要的环境，如切换到`test`环境：

```bash
conda activate test
```

![6w3rJH.md.png](https://s3.ax1x.com/2021/03/13/6w3rJH.md.png)

有了环境我们就可以在里面安装所需的包了，如安装Numpy：

```bash
conda install numpy
```

可以注意到，在这里conda和pip 的使用方法是完全相同的。

### 常用命令

到此为止Anaconda的基本用法已经描述完毕，你可以在下面的列表中找到更多常用命令：

```bash	
conda activate // 切换到base环境

conda activate test // 切换到test环境

conda create -n test python=3 // 创建一个名为test的环境并指定python版本为3的最新版本

conda env list // 列出conda管理的所有环境

conda list // 列出当前环境的所有包

conda install numpy //安装numpy包

conda remove numpy //卸载numpy包

conda remove -n test --all // 删除test环境及下属所有包

conda update numpy //更新numpy包

conda env export > environment.yaml // 导出当前环境的包信息

conda env create -f environment.yaml // 用配置文件创建新的虚拟环境

conda init <SHELL_NAME> //首次使用时，在shell中初始化Anaconda

conda config --set auto_activate_base false // 取消打开命令行时，默认进入conda环境

```

### 环境变量

如果安装时没有默认勾选使用`conda`环境变量，则可以手动配置以下环境变量，注意配置完成后一定要先重启，再使用，否则可能遇到玄学的Windows管理员权限问题。

```
//环境变量，以笔者设备为例，设置为Anaconda的安装位置

ANACONDA_HOME=C:\ProgramData\Anaconda3 


//Path列表

%ANACONDA_HOME%

%ANACONDA_HOME%\Scripts

%ANACONDA_HOME%\Library\bin

%ANACONDA_HOME%\Library\mingw-w64\bin

```

## 请参阅

+ https://www.jianshu.com/p/eaee1fadc1e9
+ https://cloud.tencent.com/developer/article/1406417