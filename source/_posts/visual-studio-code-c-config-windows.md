---
title: Windows下的Visual Studio Code 配置指南（C/C++）
date: 2020-09-11 20:44:34
tags: 
    - 教程
    - Windows
    - Visual Studio Code
    - C/C++
categories:
    - 教程
cover: https://s3.ax1x.com/2021/02/28/69DKSO.png
---


现代vscode开发环境的搭建以[Yukko](https://yukkohzq.github.io/)的“编译器版本越新越好”为嚆矢。滥觞于C与C++的传统vscode配置方案正失去它们的借鉴意义。但面对看似无垠的未来天空，我想循全新的vscode的自动配置设置，好过过早地振翮。

本篇部分内容参考[这篇文章](https://blog.csdn.net/bat67/article/details/81268581)，仅限学习用途


## 先决条件

### 下载尽可能新版的MinGW
过程略，详情请咨询[Yukko](https://yukkohzq.github.io/)

### 配置环境变量
以编译器路径为 **C:\MinGW** 为例，将其中的bin文件夹路径 **C:\MinGW\bin** 添加至 **Path** 即可（示意图仅供参考，请以实际路径为准）

![配置环境变量](https://s1.ax1x.com/2020/09/18/w4qzYF.png)

## Visual Studio Code 安装与配置

### 下载与安装 Visual Studio Code
过程略

### 安装 VSCode C/C++ 插件

![vscode-cpp](https://s1.ax1x.com/2020/09/18/w4q2Lt.png)

### 配置.vscode文件
与传统方法不同，基于新版vscode的特性，**.vscode** 中的文件可以自动配置
#### 运行
1. 在任意位置新建文件夹，创建一个 **.cpp** 文件并编写一个简单的程序（以 **a.cpp** 为例）
![新建](https://s1.ax1x.com/2020/09/11/wNLMf1.png)
![a.cpp](https://s1.ax1x.com/2020/09/11/wNOs81.png)
2. 按下 **Ctrl+Shift+B** 以进行编译，在弹出菜单中选择 **g++** 项
![编译](https://s1.ax1x.com/2020/09/11/wNOT2t.png)
3. 创建一个新的终端，在指定路径输入 **.\a** 即可运行
![run](https://s1.ax1x.com/2020/09/11/wNXSGn.png)

#### 调试
1. 添加断点

![断点](https://s1.ax1x.com/2020/09/11/wNXfyV.png)

2. 选择 **运行和调试**

![debug](https://s1.ax1x.com/2020/09/11/wNX3ZD.png)

3. 在弹出的菜单中选择**C++(GDB/LLDB)** -> **g++.exe**项，vscode将自动生成一份 **launch.json** 配置文件（只有正确配置环境变量才能生成正确的配置文件）

![gdb](https://s1.ax1x.com/2020/09/11/wNjklt.png)

![g++](https://s1.ax1x.com/2020/09/11/wNvEC9.png)

![launch.json](https://s1.ax1x.com/2020/09/11/wNvLqK.png)

4. 点击 **绿色三角形** 开始调试

![debug](https://s1.ax1x.com/2020/09/11/wNxmGj.png)

5. vscode的调试功能支持单步调试，单步跳过，堆栈信息查看等

![debug_success](https://s1.ax1x.com/2020/09/11/wNxQs0.png)

## 后续步骤

到此为止，你完成了vscode在Windows下针对C++编程的基本配置。

希望这篇文章对你有帮助，你也可以将其推荐给你使用C++编写程序却不了解如何在vscode上进行配置的朋友，希望可以让他们在vscode的配置上少走一些弯路.